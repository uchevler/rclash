import pygame, sys,math, time
from pygame.locals import *
#import msvcrt   # Windows only

AssertionsFile =  open ("AssertionInps.txt","r")
PartitionsFile =  open ("Partitions.txt","w")


pygame.init() 

#Parameters defined here ...

# set up the colors
BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
RED   = (255,   0,   0)
GREEN = (  0, 255,   0)
BLUE  = (  0,   0, 255)

#setup parameters for the graphics screen
SCR_X   = 1800
SCR_Y   = 1000
X_MARGIN= 20
Y_MARGIN= 20

IDG_X_UL= 100
IDG_Y_UL= 100

IDG_X_DR= 1500
IDG_Y_DR= 300

FONT_SIZE= 30

ADG_X_CENTER = 500
ADG_Y_CENTER = 700
ADG_RADIOUS = 270

Y_NODES_IDG = SCR_Y/2-Y_MARGIN   
Y_INPS_IDG  = 0  

NumOfRecRegInps = 8


#========= Class Definiitions =====================================================================
# define the assertion class here...
class Assertion: 
    AsString        = ""
    AsDescription   = ""
    AsName          = ""
    AsInputs        = [] # inputs appear as strings in this list
    Neighbors       = [] # the neighbors are in the form of (assertion,[list of neighbor-assertions])
    Partition       = [] # this list includes the assertions in the possible partitions 


#define the assertion-set class here ...

class AssertionSet:
    Set = []


class IDG_TYPE: 
    Assertions=[]
    AllInputs=[]
    

class ADG_TYPE: 
    Assertions=[]
    NumOfCommInp=[]
 


#========== End of Class Definitions  =================================================

#======== Function Definitions =================================================================================

# wait function in Windows
def wait():
    msvcrt.getch()


# a function to find common members of two lists
def CommonMembers(list1,list2):
    CommonMems = [x for x in list1 if x in list2]          
    return CommonMems


def InitGraphs(AssertionsFile):
    # ================== Read assertions from a file and fill out the data structure for the whole assertion-set ==================

    # Instances
    a1 = Assertion()
    IDG = IDG_TYPE()






    # Build up a set of assertions from a text-file here ...    
    for line in AssertionsFile:  
        a1          = Assertion()
        a1.AsName   = (line.split())[0]
        a1.AsInputs = (line.split())[1:] 
        IDG.Assertions = IDG.Assertions + [a1]  
        a1.Partition   = a1.Partition + [a1.AsName]

    # Build up the set of inputs for the assertion set  
    for i in xrange (0,len (IDG.Assertions)):
        for j in xrange (0,len (IDG.Assertions[i].AsInputs)):
            if not (IDG.Assertions[i].AsInputs[j] in IDG.AllInputs):
                IDG.AllInputs = IDG.AllInputs + [IDG.Assertions[i].AsInputs[j]]
    # Print the list of all assertion-inputs
    #print [x for x in IDG.AllInputs]


    # Build up neighbors for the assertions in the assertion-set here ...
    for i in xrange (0,len(IDG.Assertions)):
        for j in xrange (0,len(IDG.Assertions)):
            if i!=j:
                CommInps = CommonMembers(IDG.Assertions[i].AsInputs,IDG.Assertions[j].AsInputs)
                if len(CommInps)!=0:
                    IDG.Assertions[i].Neighbors = IDG.Assertions[i].Neighbors + [(IDG.Assertions[j],CommInps)]



    # A test print of neighbors of each assertion
    #for i in xrange (0,len(IDG.Assertions)):
        #print "Neigbors of assertion %s are: \n" %(IDG.Assertions[i].AsName)
        #for j in xrange (0,len(IDG.Assertions[i].Neighbors)):        
            #print IDG.Assertions[i].Neighbors[j][0].AsName, IDG.Assertions[i].Neighbors[j][1]
        #print "initial partitions are: %s \n" %(IDG.Assertions[i].Partition)    
        #print "\n"   
    return IDG



# The graph-drawing function. It gets an IDG and draws it in the specified position passed as an argument.
def DrawIDG(IDG,screen, UpLeftPoint, DownRightPoint ):
    # draw nodes in a for loop
    XLength = DownRightPoint[0] - UpLeftPoint[0]
    YLength = DownRightPoint[1] - UpLeftPoint[1]
    font = pygame.font.Font(None, FONT_SIZE)

    for i in xrange(0, len(IDG.Assertions)):
        # print the name of the assertion
        text = font.render(IDG.Assertions[i].AsName, 1, WHITE)
        textpos = text.get_rect()

        (textpos.centerx,textpos.centery) = (UpLeftPoint[0] + i*XLength/len(IDG.Assertions), UpLeftPoint[1]+YLength+15)
        screen.blit(text, textpos)
        # draw the circle for the node
        pygame.draw.circle (screen, WHITE, (UpLeftPoint[0] + i*XLength/len(IDG.Assertions),UpLeftPoint[1]+YLength), 5, 4)
            


    # draw inputs in a for loop
    for i in xrange(0, len(IDG.AllInputs)):
        # print the name of the assertion
        text = font.render(IDG.AllInputs[i], 1, WHITE)
        textpos = text.get_rect()

        (textpos.centerx,textpos.centery) = (UpLeftPoint[0] + i*XLength/len(IDG.AllInputs),UpLeftPoint[1])
        screen.blit(text, textpos)
        # draw the circle for the node
        pygame.draw.circle (screen, WHITE, (UpLeftPoint[0] + i*XLength/len(IDG.AllInputs),UpLeftPoint[1]+15), 5, 4)
        


    # draw edges between nodes 
    for i in xrange (0,len (IDG.Assertions)):
        for j in xrange (0,len (IDG.AllInputs)):
            if (IDG.AllInputs[j] in IDG.Assertions[i].AsInputs):
                # Draw a line from assertion[i] to input[j]
                pygame.draw.line(screen, (255, 255, 255), (UpLeftPoint[0] + i*XLength/len(IDG.Assertions),UpLeftPoint[1]+YLength), (UpLeftPoint[0] + j*XLength/len(IDG.AllInputs),UpLeftPoint[1]+15) )



    pygame.display.flip()
        

# The graph-drawing function. It gets an ADG and draws it in the specified position passed as an argument.
def DrawADG(ADG,screen, CenterPoint,Radious ):
    
    
    angles = [x*360/len(ADG.Assertions) for x in xrange(0,len(ADG.Assertions))]
    x_coordinates = [int (CenterPoint[0] + Radious*math.cos(math.pi*x/180)) for x in angles]     
    y_coordinates = [int (CenterPoint[1] + Radious*math.sin(math.pi*x/180)) for x in angles]

    font = pygame.font.Font(None, FONT_SIZE)
    
    #draw the nodes on a circle
    for i in xrange(0, len(ADG.Assertions)):
        pygame.draw.circle (screen, WHITE, (x_coordinates[i],y_coordinates[i]), 5, 4)         
        # put the assertion-names on each node
        text = font.render( ADG.Assertions[i].AsName, 1, RED)
        textpos = text.get_rect()
                        
        text_x = int (x_coordinates[i] - (CenterPoint[0] - x_coordinates[i]) / 12 )
        text_y = int (y_coordinates[i] - (CenterPoint[1] - y_coordinates[i]) / 12 )
        (textpos.centerx,textpos.centery) = (text_x,text_y)
        screen.blit(text, textpos)

    
    # draw edges between nodes 
    for i in xrange (0,len (ADG.Assertions)):
        for j in xrange (i,len (ADG.Assertions)):
                
                # Check if assertion_i is a neighbor of assertion_j, if yes, draw an edge between them            
                neighbors_j = [x for (x,y) in ADG.Assertions[j].Neighbors]
                if ADG.Assertions[i] in neighbors_j:                 
                    # Draw a line from assertion[i] to input[j]
                    pygame.draw.line(screen, (255, 255, 255), (x_coordinates[i],y_coordinates[i]),(x_coordinates[j],y_coordinates[j]) )
                    # Write the number of common inputs on the ADG graph
                    text = font.render( str (len (CommonMembers(ADG.Assertions[i].AsInputs,ADG.Assertions[j].AsInputs))), 1, RED)
                    textpos = text.get_rect()
                
                    text_x = int (x_coordinates[i] + (x_coordinates[j] - x_coordinates[i]) / 8 )
                    text_y = int (y_coordinates[i] + (y_coordinates[j] - y_coordinates[i]) / 8 )
                    (textpos.centerx,textpos.centery) = (text_x,text_y)
                    screen.blit(text, textpos)

                     




    pygame.display.flip()
        

# this function finds the candidate for combining by using the Best-Fit approach
def FindBestFitPartitions(IDG,NumOfRecRegInps): 

    BestFitPair = (IDG.Assertions[0],IDG.Assertions[0])

    # a stupid large number :)
    MinNumOfInps=  999999999
    
    index_i = 0
    index_j = 0

# find the partition with most empty places for inputs, most suitable to combine with other partitions    
    for i in xrange (0, len(IDG.Assertions)):
        if ( MinNumOfInps > len(IDG.Assertions[i].AsInputs) ):
            index_i = i
    index_j = index_i
    slack =  NumOfRecRegInps - len(IDG.Assertions[i].AsInputs)       
# find the partition which is the most suitable to merge with previous one ...    
    
    # the stupid large number, again :)
    MinNumOfInps=  999999999

    for j in xrange (0, len(IDG.Assertions)):
        if ( j!= index_i ):
            if (MinNumOfInps > len(IDG.Assertions[j].AsInputs))  and (len(IDG.Assertions[j].AsInputs) <= slack):
             index_j = j

    
    if (index_j != index_i): # no fit found!
        return ((index_i,index_j), False)          
    else: 
        return ((-1,-1), True)    


# this functions merges the Best-Fit Pair   
def MergeBestFit(IDG,BestFitPair):
    
    # make a new node to replace two most connected nodes...
    NewNode = Assertion()
    UpdatedIDG = IDG_TYPE()
    UpdatedIDG.AllInputs = IDG.AllInputs 
    #combine the names of the two most-connected nodes
    NewNode.AsName = IDG.Assertions[BestFitPair[0]].AsName + IDG.Assertions[BestFitPair[1]].AsName

    # combine the partitions of the most-connected nodes
    NewNode.Partition = IDG.Assertions[BestFitPair[0]].Partition + IDG.Assertions[BestFitPair[1]].Partition
    
    # make the list of inputs for aiaj combined node
    NewNode.AsInputs = IDG.Assertions[BestFitPair[0]].AsInputs
    for x in IDG.Assertions[BestFitPair[1]].AsInputs:
        # if the neighbor is not one of the merged nodes or it is not already there, add it to neighbor list of the merged node
        if  not( x in NewNode.AsInputs):
            NewNode.AsInputs = NewNode.AsInputs + [x]
    
    # remove a_i and a_j from the assertion-list and add the new node to it.
    IDG.Assertions.pop(BestFitPair[1])
    IDG.Assertions.pop(BestFitPair[0])
    UpdatedIDG.Assertions = IDG.Assertions
    #for a in  UpdatedIDG.Assertions:
        #print a.AsName
    # add the new node to the new assertion-list
    UpdatedIDG.Assertions =  UpdatedIDG.Assertions + [NewNode]
    # empty the neighbor list for each assertion in order to make a new one
    for a in UpdatedIDG.Assertions:
        a.Neighbors = []
 
    
    # Build up neighbors for the assertions in the assertion-set here ...

    
    for i in xrange (0,len(UpdatedIDG.Assertions)):
        for j in xrange (0,len(UpdatedIDG.Assertions)):
            if i!=j:
                CommInps = CommonMembers(UpdatedIDG.Assertions[i].AsInputs,UpdatedIDG.Assertions[j].AsInputs)
                if len(CommInps)!=0:
                    UpdatedIDG.Assertions[i].Neighbors = UpdatedIDG.Assertions[i].Neighbors + [(UpdatedIDG.Assertions[j],CommInps)]


   
    
    return (UpdatedIDG)
    







def FindMostConnected(IDG,NumOfRecRegInps): 
    FinishCondition = True
    MostConnected = (IDG.Assertions[0],IDG.Assertions[0])
    MaxWeight     =  -1
    
    for a in IDG.Assertions:
        for n in a.Neighbors:
            
             if ( ( MaxWeight < len (n[1])) and   ( NumOfRecRegInps >= (len(a.AsInputs) + len( (n[0]).AsInputs) - len( (n[1])) ))  ): 
                MaxWeight = len (n[1])
                MostConnected = (a.AsName,n[0].AsName)
                FinishCondition = False

    if not (FinishCondition):
        for i in xrange (0,len(IDG.Assertions)):
            if (IDG.Assertions[i].AsName == (MostConnected[0])):
                index_i = i            
            if (IDG.Assertions[i].AsName == (MostConnected[1])):
                index_j = i            
    
    if not FinishCondition:
        return ((index_i,index_j), FinishCondition)          
    else: 
        return ((-1,-1), True)     
def MergeMostConnected(IDG,MostConnectedPair):
    
    # make a new node to replace two most connected nodes...
    NewNode = Assertion()
    UpdatedIDG = IDG_TYPE()
    UpdatedIDG.AllInputs = IDG.AllInputs 
    #combine the names of the two most-connected nodes
    NewNode.AsName = IDG.Assertions[MostConnectedPair[0]].AsName + IDG.Assertions[MostConnectedPair[1]].AsName

    # combine the partitions of the most-connected nodes
    NewNode.Partition = IDG.Assertions[MostConnectedPair[0]].Partition + IDG.Assertions[MostConnectedPair[1]].Partition
    
    # make the list of inputs for aiaj combined node
    NewNode.AsInputs = IDG.Assertions[MostConnectedPair[0]].AsInputs
    for x in IDG.Assertions[MostConnectedPair[1]].AsInputs:
        # if the neighbor is not one of the merged nodes or it is not already there, add it to neighbor list of the merged node
        if  not( x in NewNode.AsInputs):
            NewNode.AsInputs = NewNode.AsInputs + [x]
    
    # remove a_i and a_j from the assertion-list and add the new node to it.
    IDG.Assertions.pop(MostConnectedPair[1])
    IDG.Assertions.pop(MostConnectedPair[0])
    UpdatedIDG.Assertions = IDG.Assertions
    #for a in  UpdatedIDG.Assertions:
        #print a.AsName
    # add the new node to the new assertion-list
    UpdatedIDG.Assertions =  UpdatedIDG.Assertions + [NewNode]
    # empty the neighbor list for each assertion in order to make a new one
    for a in UpdatedIDG.Assertions:
        a.Neighbors = []
 
    
    # Build up neighbors for the assertions in the assertion-set here ...

    
    for i in xrange (0,len(UpdatedIDG.Assertions)):
        for j in xrange (0,len(UpdatedIDG.Assertions)):
            if i!=j:
                CommInps = CommonMembers(UpdatedIDG.Assertions[i].AsInputs,UpdatedIDG.Assertions[j].AsInputs)
                if len(CommInps)!=0:
                    UpdatedIDG.Assertions[i].Neighbors = UpdatedIDG.Assertions[i].Neighbors + [(UpdatedIDG.Assertions[j],CommInps)]


   
    
    return (UpdatedIDG)
    


#========= Enf of Function Definitions  ===========================================================



screen = pygame.display.set_mode((SCR_X, SCR_Y)) 
FinishCondition = False

# in the init function, the graphs are built based on the input assertion file
IDG = InitGraphs(AssertionsFile)
ADG = ADG_TYPE() 
#The main loop of the partitioning algorithm which plots the Input Dependency Graph (IDG) here ...
time.sleep(1)
while not (FinishCondition):
        
    
        DrawIDG (IDG,screen, (IDG_X_UL,IDG_Y_UL), (IDG_X_DR,IDG_Y_DR) )

        # build up the ADG graph here ...

        ADG.Assertions = IDG.Assertions

        DrawADG (ADG,screen, (ADG_X_CENTER,ADG_Y_CENTER), ADG_RADIOUS )

        time.sleep(0.1)
        
       
        # find the most-connected pair
        (MostConnectedPair,FinishCondition) = FindMostConnected(IDG,NumOfRecRegInps)


        #change the IDG
        if not FinishCondition:        
            IDG= MergeMostConnected(IDG,MostConnectedPair) 
            screen.fill( (0,0,0) )
            pygame.display.flip()              



screen.fill( (0,0,0) )
pygame.display.flip()              

# run a best-fit packing algorithm to combine unconnected partitions ...
FinishCondition = False
while not (FinishCondition):
        
    
        DrawIDG (IDG,screen, (IDG_X_UL,IDG_Y_UL), (IDG_X_DR,IDG_Y_DR) )

        # build up the ADG graph here ...

        ADG.Assertions = IDG.Assertions

        DrawADG (ADG,screen, (ADG_X_CENTER,ADG_Y_CENTER), ADG_RADIOUS )

        time.sleep(0.1)
        
       
        # find the most-connected pair
        (BestFitPair,FinishCondition) = FindBestFitPartitions(IDG,NumOfRecRegInps)


        #change the IDG
        if not FinishCondition:        
            IDG= MergeBestFit(IDG,BestFitPair) 
            screen.fill( (0,0,0) )
            pygame.display.flip()              


# write the partitions into the output file


for i in IDG.Assertions:
    PartitionsFile.write(str(i.Partition))
    PartitionsFile.write("\n")
    
PartitionsFile.close()
raw_input(" Partitioning is finished, press the Enter key to return to Python ...")         
