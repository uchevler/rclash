# Module Name   : rtrclash.py script
# Author        : Bahram Najafiuchevler
# Created       : 09.11.2014
# Last Modified : 20.11.2016

# Description:
# This script runs the folowing steps:
#    1-Runs CLaSH on the RTR design to generate the VHDL/Verilog files
#    2-Extracts the Reconfigurable Modules (Reconfiguration Can didates) as well as the static design
#    3-Copies the Reconfiguration-related files into the template-project structure and change the design.tcl script of the reconfiguration flow
#    4-Runs the Vivado tool with the updated/changed/added design files and constraints, to generate partial and full bitstreams. 
#    5-Generate the reconfiguration management software to control the reconfiguration flow.


import pygame, sys,math
import sys, string, os
from subprocess import Popen, PIPE
from pygame.locals import *
from subprocess import call
from glob import glob  # to support wildcard in os.remove() argument
from shutil import copyfile # to copy files between directories
from shutil import rmtree # to copy files between directories
from distutils.dir_util import copy_tree
# ***********************************************************************************************
#                                   Configurations and Settings
# ***********************************************************************************************

# setting for the Haskell description of the RTR design

# uncomment for simple RTR counter design
#RTR_HASKELL_DESIGN_HOME = "/home/uchi/PhDInOslo/phd/clash_designs/rtrcounters/" 
#RTR_HASKELL_DESIGN_NAME = "rtr_example_counters.hs"
#RTR_WRAPPER_NAME = "topWrapper"
#TOP_CLASH_MODULE_NAME = "RTRCounter"


# uncomment for the case study design (AHB-based design)
RTR_HASKELL_DESIGN_HOME = "/home/uchi/PhDInOslo/phd/clash_designs/casestudy/"  
RTR_HASKELL_DESIGN_NAME = "AHBSystem.hs"
RTR_WRAPPER_NAME = "topWrapper"
TOP_CLASH_MODULE_NAME = "AHBSystem"

RTR_HASKELL_DESIGN_SIM_HOME =  RTR_HASKELL_DESIGN_HOME + "sim/"
RTR_HASKELL_DESIGN_SYN_HOME =  RTR_HASKELL_DESIGN_HOME + "syn/"
RTR_VHDL_DESIGN_SIM_HOME = RTR_HASKELL_DESIGN_SIM_HOME + "vhdl/"
RTR_HASKELL_DESIGN_FULL_PATH = RTR_HASKELL_DESIGN_SIM_HOME +  RTR_HASKELL_DESIGN_NAME
RTR_HASKELL_DESIGN_FULL_PATH_SYN = RTR_HASKELL_DESIGN_SYN_HOME +  RTR_HASKELL_DESIGN_NAME
HDL = "VHDL" #Choose VHDL or Verilog 

# path to search for clash source files and other Haskell source files. seperate multiple paths by ":"
RTR_CLASH_INCLUDE_PATH = "/home/uchi/PhDInOslo/phd/clash_designs/rtrinclude/" 
VIVADO_TEMPLATE_PRJ = "/home/uchi/PhDInOslo/phd/zynq_rec_design_template"

# The CLaSH command used for compiling/loading and generating HDL files.
FULL_CLASH_COMMAND = "clash " + RTR_HASKELL_DESIGN_FULL_PATH  + " --" + HDL.lower() + " -i" + RTR_CLASH_INCLUDE_PATH
SYN_CLASH_COMMAND = "clash " + RTR_HASKELL_DESIGN_FULL_PATH_SYN  + " --" + HDL.lower() 
# Settings for the Vivado project of the RTR design
RTR_VIVADO_DESIGN_HOME = RTR_HASKELL_DESIGN_HOME + "vivado_prj/vivado_pr/"
FULL_VIVADO_COMMAND = "vivado -mode tcl -nolog -notrace -source scripts/design.tcl"
VERBOSE = 1

# ***********************************************************************************************
#  Data-Structures 
# ***********************************************************************************************
class RecRegion: 
    InType  =""  
    InSize    = 0
    OutType   = ""
    OutSize  = 0
    RecModules = []
    NumOfRecModules = 0
    RMVector = ""
    RecRegHierarchy= []
    topModule = ""
    RecRegionName = ""
    RTRDelay = "100 ns"
    TimeUnit = "us"
    Location = ""
    HDLHierarchy = []
# [  [toptest4 , 0]  ,  [recmodule1, 0]  ,  [recReg, 0]  ] : recReg --> recReg0
# [  [toptest4 , 1]  ,  [recmodule2, 2]  ,  [recReg, 0]  ] : recReg --> recReg1

class SourceFile:
    fileName= ""
    srcCode= []


# ***********************************************************************************************
#  Helper Functions 
# ***********************************************************************************************

#    This procedure finds the name of the parent module for the module passed in the argument. It uses the 
# identation of expressions in Haskell to find the parent module name

def makeSimDir ():

    # Make sim directory in the project path
    sim_path = RTR_HASKELL_DESIGN_HOME + "sim"


    if not os.path.exists(sim_path):
        print ("Creating simulation directory ...")
        os.makedirs(sim_path)
        result = 1
def makeSynDir ():

    # Make sim directory in the project path
    sim_path = RTR_HASKELL_DESIGN_HOME + "syn"


    if not os.path.exists(sim_path):
        print ("Creating sythesis directory ...")
        os.makedirs(sim_path)
        result = 1
        
def makeDir (path):
    if not os.path.exists(path):
        print ("Creating directory " + path + "...")
        os.makedirs(path)
        result = 1

def deleteDir(path):
    if os.path.exists(path):
        rmtree(path)

    
def populateSimDir(srcArray):    

    # Path for temporary CLaSH souce files
    temp_path = RTR_HASKELL_DESIGN_HOME + "sim/"

    print ("Creating temporary CLaSH source files for simulation ...")

    # write contents of the source-array into files
    for i in xrange (0, len (srcArray)):
        src_file = open (temp_path + srcArray[i].fileName ,"w")
        for j in xrange (0, len (srcArray[i].srcCode)):
            src_file.write(srcArray[i].srcCode[j])
            
        src_file.close()    
        print "        Created source file " + temp_path + srcArray[i].fileName        

    # Copy the library files into the sim directory
    
    if (VERBOSE):
        print "Copying isolation cell CLaSH and json files ... "
    src = RTR_CLASH_INCLUDE_PATH + "RecReg.hs"
    dst = RTR_HASKELL_DESIGN_SIM_HOME + "RecReg.hs"
    copyfile(src, dst) 
    

    src = RTR_CLASH_INCLUDE_PATH + "RecRegLib.hs"
    dst = RTR_HASKELL_DESIGN_SIM_HOME  + "RecRegLib.hs"
    copyfile(src, dst)   

    src = RTR_CLASH_INCLUDE_PATH + "RecRegLib.json"
    dst = RTR_HASKELL_DESIGN_SIM_HOME + "RecRegLib.json"  
    copyfile(src, dst)  
    if (VERBOSE):
        print "Done!"

def populateSynDir(srcArray):    

    # Path for temporary CLaSH souce files
    temp_path = RTR_HASKELL_DESIGN_HOME + "syn/"

    print ("Creating temporary CLaSH source files for synthesis ...")

    # write contents of the source-array into files
    for i in xrange (0, len (srcArray)):
        src_file = open (temp_path + srcArray[i].fileName ,"w")
        for j in xrange (0, len (srcArray[i].srcCode)):
            templine = srcArray[i].srcCode[j]
            tokens = templine.split()
            if len(templine) > 8:
                if templine[0:6] == "recReg" and tokens[1] == "sel" and tokens[2] == "inp":
                    src_file.write(templine.replace("recReg","recReg_"))
                else:
                    src_file.write(srcArray[i].srcCode[j])    
            else:
                src_file.write(srcArray[i].srcCode[j])
        src_file.close()    
        print "        Created source file " + temp_path + srcArray[i].fileName        

    # Copy the library files into the sim directory
    
    src = RTR_CLASH_INCLUDE_PATH + "RecReg.hs"
    dst = RTR_HASKELL_DESIGN_SYN_HOME + "RecReg.hs"
    copyfile(src, dst) 
    

    src = RTR_CLASH_INCLUDE_PATH + "syn/RecRegLib.hs"
    dst = RTR_HASKELL_DESIGN_SYN_HOME  + "RecRegLib.hs"
    copyfile(src, dst)   

    src = RTR_CLASH_INCLUDE_PATH + "syn/RecRegLib.json"
    dst = RTR_HASKELL_DESIGN_SYN_HOME + "RecRegLib.json"  
    copyfile(src, dst)  

def removeRecRegDef(srcArray):    
    newSrcArray = srcArray
    # Path for temporary CLaSH souce files
    temp_path = RTR_HASKELL_DESIGN_HOME + "syn/"

    print ("Creating temporary CLaSH source files for synthesis ...")

    # write contents of the source-array into files
    for i in xrange (0, len (srcArray)):
        for j in xrange (0, len (srcArray[i].srcCode)):
            templine = srcArray[i].srcCode[j]
            tokens = templine.split()
            if len(templine) > 8:
                if templine[0:6] == "recReg" and tokens[1] == "sel" and tokens[2] == "inp":
                    newSrcArray[i].srcCode[j] = templine.replace("recReg","recReg_")
    return newSrcArray                                   
def populateDir(srcArray):   

    # write contents of the source-array into files
    for i in xrange (0, len (srcArray)):
        src_file = open (srcArray[i].fileName ,"w")
        for j in xrange (0, len (srcArray[i].srcCode)):
            src_file.write(srcArray[i].srcCode[j])
            
        src_file.close()
    
def findRecWrapper():

    return recWrapperFile

def findFiles(path, extension):
    listOfFiles= []
    for file in os.listdir(path):
        if file.endswith(extension):            
            listOfFiles = listOfFiles + [file]

    return listOfFiles

def findVHDLFiles():
    listOfFiles= []
    for file in os.listdir(RTR_HASKELL_DESIGN_HOME + "syn/vhdl/" + TOP_CLASH_MODULE_NAME):
        if file.endswith(".vhdl"):            
            listOfFiles = listOfFiles + [file]

    return listOfFiles

def readHDLFiles():
    sourceFiles = findVHDLFiles()
    srcArray = []
    for i in xrange (0 , len(sourceFiles)):
        # make an array to fill with the file data
        sf= SourceFile()
        sf.fileName =  sourceFiles[i]
        # open the file and read in the contents into an array of string
        src_file = open (RTR_HASKELL_DESIGN_HOME + "syn/vhdl/" + TOP_CLASH_MODULE_NAME + "/" + sourceFiles[i],"r")
        for line in src_file:
            sf.srcCode = sf.srcCode + [str(line)]

        if (VERBOSE == 1):
            print ("    File " + sourceFiles[i] + " is read" )

        # close the file
        src_file.close()
        # add the file src-array to the list of src-arrays
        srcArray =  srcArray + [sf] 
    return srcArray

def findSourceFiles():
    listOfFiles= []
    for file in os.listdir(RTR_HASKELL_DESIGN_HOME):
        if file.endswith(".hs"):            
            listOfFiles = listOfFiles + [file]

    return listOfFiles

def fillDataStructure():
    sourceFiles = findSourceFiles()
    srcArray = []
    for i in xrange (0 , len(sourceFiles)):
        # make an array to fill with the file data
        sf= SourceFile()
        sf.fileName =  sourceFiles[i]
        # open the file and read in the contents into an array of string
        src_file = open (RTR_HASKELL_DESIGN_HOME + sourceFiles[i],"r")
        for line in src_file:
            sf.srcCode = sf.srcCode + [str(line)]

        if (VERBOSE == 1):
            print ("    File " + sourceFiles[i] + " is read" )

        #close the file
        src_file.close()
        # add the file src-array to the list of src-arrays
        srcArray =  srcArray + [sf] 
    return srcArray

def replaceStrings(replaceList, string):
    result = string
    for i in xrange (0 , len(replaceList)):
        result = result.replace(replaceList[i][0],replaceList[i][1])
    return result

def isComment(string):
    result =  replaceStrings([[" ",""]] , string)
    if result[0:2] == "--":
        return 1
    else:
        return 0

def commentWrapper(srcArray,top):
    newSrcArray = srcArray
    
    for i in xrange (0 , len(srcArray)):        
        wrapper_found = 0
        for j in xrange (0 , len(srcArray[i].srcCode)):
            if isComment(srcArray[i].srcCode[j]) == 0 :
                templine = srcArray[i].srcCode[j]
                tokens = templine.split()
                wrapper_indent = indentation(templine)
                if  len(tokens) > 1:
                    if tokens[0] == RTR_WRAPPER_NAME:
                        newSrcArray[i].srcCode [j] = RTR_WRAPPER_NAME + " = " + top 
                        #newSrcArray[i].srcCode[j] = "--" + srcArray[i].srcCode[j]
                        wrapper_found = 1                             

    return newSrcArray

# This procedure finds the existing reconfigurable regions using the reconfigurable types
def findRecRegions(srcArray):
    recRegs = []    
    for i in xrange (0 , len(srcArray)):
        for j in xrange (0 , len(srcArray[i].srcCode)):
            if (srcArray[i].srcCode[j]).find("RecRegSigIOType") != -1 and (srcArray[i].srcCode[j]).find("Vec ") != -1 :
                if srcArray[i].srcCode[j].index("Vec ") < srcArray[i].srcCode[j].index("RecRegSigIOType"): # a type declaration for a rec reg           
                
                    if isComment(srcArray[i].srcCode[j]) == 0: # if it is not a cemment line                
                        recRegion = RecRegion()
                        lineRefined = replaceStrings([ ["("," "],[")"," "],["->"," "],["::"," "]  ], srcArray[i].srcCode[j])
                        lineTokens = lineRefined.split()
                        
                        InTypeFound = 0
                        for k in xrange (0,len(lineTokens)):
                            if lineTokens[k].find("RecRegSigIOType") != -1: # found the input type
                                if InTypeFound == 0:
                                    recRegion.InType = lineTokens[k]
                                    recRegion.InSize = int ( (recRegion.InType).replace("RecRegSigIOType",""))
                                    InTypeFound = 1
                                else:
                                    recRegion.OutType = lineTokens[k]
                                    recRegion.OutSize = int ( (recRegion.OutType).replace("RecRegSigIOType",""))
                                print  lineTokens   
                                recRegion.NumOfRecModules = int (lineTokens[2])
                                recRegion.RMVector = lineTokens[0]
                        recRegion.RecRegionName = "recReg" + str(len(recRegs))
                        recRegs = recRegs + [recRegion]        
        # look for the reconfigurable modules in the vector
        for j in xrange (0 , len(srcArray[i].srcCode)):
            for recindex in xrange (0 , len(recRegs)):
                templine = (srcArray[i].srcCode[j]).replace (" ","")
                if templine.find(recRegs[recindex].RMVector) != -1: 
                    if templine[0:len(recRegs[recindex].RMVector) + 1] == (recRegs[recindex].RMVector + "="): # a declaration for a rec vector
                        if isComment(srcArray[i].srcCode[j]) == 0: # if it is not a cemment line  
                            lineRefined = replaceStrings([ ["("," "],[")"," "],[":>"," "],["Nil"," "],["="," "]  ], srcArray[i].srcCode[j])
                            lineTokens = lineRefined.split()

                            # extract the RMs from the vector
                            for vecindex in xrange (0 , recRegs[recindex].NumOfRecModules):
                                recRegs[recindex].RecModules = recRegs[recindex].RecModules + [lineTokens[1+vecindex]]
                            
                                
    return recRegs

# This function finds the top module
def findRecTop(srcArray):
    for i in xrange (0 , len(srcArray)):
        for j in xrange (0 , len(srcArray[i].srcCode)):
            if len(srcArray[i].srcCode[j]) > len (RTR_WRAPPER_NAME) + 2 and len(srcArray[i].srcCode[j].split()) > 3:
                templine1 = (srcArray[i].srcCode[j]).split()
                if templine1[0] == RTR_WRAPPER_NAME: # a declaration for a top_wrapper
                    if (srcArray[i].srcCode[j]).find("=") != -1:  # it is the definition of the top_wrapper
                        if isComment(srcArray[i].srcCode[j]) == 0: # if it is not a cemment line  
                            templine2 = (srcArray[i].srcCode[j]) [(srcArray[i].srcCode[j]).index("=")+1:]
                            recTop = (templine2.split())[0]
                                
    return recTop

def removeArgumentCtrlChars(line):
    result = replaceStrings([ ["("," "],[")"," "],[","," "] ] , line )
    return result
    
def indentation (line): # Empty lines should be bypassed while using this function
    result = 0
    for i in xrange (0 , len(line) ):
        if line[i] == " ":
            result = result + 1
        else:
            break     
    return result

def isLineEmpty(line):
    result = 0
    templine = line.replace(" ", "")
    if len(templine) < 1:
        result = 1
    return result 
       
def isFunctionDefinition(line,module):
    result = 0
    templine = (replaceStrings([ ["("," "],[")"," "],[","," "] ] , line )).split()
    if len(templine) > 0:
        if templine[0] == module and line.find("=") != -1 :
            result = 1
    
    return result
def findToken (token, line):
    templine = (replaceStrings([ ["("," "],[")"," "],[","," "] ] , line )).split()
    result = 0
    for i in xrange (0 , len (templine)):
        if token == templine[i]:
            result = 1
    return result

def capitalizeFirstChar(line):
    result = line
    result = result[0].upper() + result [1:]
    return result
    
def onlyLetters(line):
    result = ""
    for i in xrange (0, len(line)):
        if (line[i] > 'a' and line[i] < 'z') or (line[i] > 'A' and line[i] < 'Z'):
            result = result + line[i]
        else:
            result = result + " "     
    return result    

def removeIndentation(line):
    result = ""
    index = 0
    
    while (line[index] == " "):
        index = index + 1
    
    result = line [index:]
         
    return result    

def isToken(name , line):
    result = 0
    tokens = line.split()
    for i in xrange (0, len(tokens)):
        if name == tokens[i] :
            result = 1            
    return result
    
def deleteSimDir():
    if os.path.exists(RTR_HASKELL_DESIGN_SIM_HOME):
        rmtree(RTR_HASKELL_DESIGN_SIM_HOME)
    

def deleteSynDir():
    if os.path.exists(RTR_HASKELL_DESIGN_SYN_HOME):
        rmtree(RTR_HASKELL_DESIGN_SYN_HOME)
    
    
def addImportLines(lines, array):
    newarray = []
    arrIndex = 0
    first_token = ""
    while first_token != "import":
        if len (array[arrIndex]):
            first_token = (array[arrIndex].split())[0] 
            newarray = newarray + [array[arrIndex]]
            arrIndex = arrIndex +1
   
    
    # the first import is found, insert the import lines after the first import        
    for i in xrange (0 , len(lines)):
        newarray = newarray + [lines [i]]
    
    # copy the rest of the array to the new array
    for i in xrange (arrIndex, len (array)):
        newarray = newarray + [array [i]]    
    return newarray     

def replaceRecType (line, newType):
    
    templine = replaceStrings([ ["("," ( "],[")"," ) "],[","," , "],["::"," :: "] ,["->"," -> "] ], line)
    tokens = templine.split()
    indent = indentation (line)
    newline = ""
    newTokens = tokens
    
    
    for t in xrange (0, len (tokens)):
        newTokens[t] = tokens[t]
        if t >=4 :
             
            if tokens[t-4] == "(" and tokens[t-3].find("RecRegSigIO") != -1 and tokens[t-2].find("->") != -1 and tokens[t-1].find("RecRegSigIO") != -1:
                newTokens[t-4] = newType
                newTokens[t-3] = ""
                newTokens[t-2] = ""
                newTokens[t-1] = ""
                newTokens[t] = ""
    for t in xrange (0, len (tokens)):
        newline = newline + newTokens[t] + " "    
    newline = newline + "\n"    
    return newline
                        

        
    return result
def addIndentation(line,indent):
    result = ""
    for i in xrange (0, indent):
        result = reslut + " "
    result = result + line    
    return result
    
def charCount(word, char):
  count = 0
  for c in word:
    if char == c:
      count += 1
  return count    
# This function changes the type of the topmodule to match the changes from 'function' to 'select line'.
def changeTopmoduleType(topModule,srcArray):
    
    newSrcArray = srcArray
    for i in xrange (0 , len(srcArray)):        
        topFound = 0
        for j in xrange (0 , len(srcArray[i].srcCode)):
            if isComment(srcArray[i].srcCode[j]) == 0 :
                if srcArray[i].srcCode[j].find("::") != -1 and srcArray[i].srcCode[j].find(topModule) != -1: # topmodule type definition
                    newline  = replaceRecType (srcArray[i].srcCode[j], " Signal (Unsigned 8) ")
                    newSrcArray[i].srcCode[j] = newline
                    topFound = 1

    return newSrcArray    
# This function finds the reconfigurable-function argument in the hierarchy.
def findRecArgInHierarchy(srcArray,recRegs):
    
    if len (recRegs) > 1 : # find the hierarchy of the reconfigurable regions only if there are more than one. not needed for one RR.
        newRecRegs = recRegs
        moduleDefIndentation = 0
        for recRegIndex in xrange(0 , len (newRecRegs)):
            newRecRegs[recRegIndex].topModule = findRecTop(srcArray)
            # Find the reconfigurble argument index for the top module first
            module_name = newRecRegs[recRegIndex].topModule        
            for i in xrange (0 , len(srcArray)):        
                for j in xrange (0 , len(srcArray[i].srcCode)):
                    if isComment(srcArray[i].srcCode[j]) == 0 :
                        if (srcArray[i].srcCode[j]).find(newRecRegs[recRegIndex].RMVector) != -1 and (srcArray[i].srcCode[j]).find(newRecRegs[recRegIndex].topModule) != -1:
                            templine1 = replaceStrings([ ["("," "],[")"," "],[","," "],["!!"," "],["="," "]  ], srcArray[i].srcCode[j])
                            tokens = templine1.split()
                            for tokenIndex in xrange (0 , len (tokens)):
                                if tokens[tokenIndex] == newRecRegs[recRegIndex].topModule:
                                    topModuleIndex = tokenIndex
                                if tokens[tokenIndex] == newRecRegs[recRegIndex].RMVector :
                                    recArgIndex = int ( ( (tokenIndex -1) - topModuleIndex ) / 2 )
            # add the top module arguments info to the list in the recReg struct
            newRecRegs[recRegIndex]. RecRegHierarchy = newRecRegs[recRegIndex]. RecRegHierarchy + [[newRecRegs[recRegIndex].topModule] + [recArgIndex] ]
            while module_name != "recReg": # look into files until it finds the recReg module in a function definition
                # Find the module in the design hierarchy
                moduleDefFound = 0
                recArgFound = 0
                for i in xrange (0 , len(srcArray)):        
                    if module_name == "recReg" :
                        break
                    for j in xrange (0 , len(srcArray[i].srcCode)):
                        if module_name == "recReg" :
                            break
                        if isComment(srcArray[i].srcCode[j]) == 0 :

                            if  moduleDefFound == 1 and  indentation (srcArray[i].srcCode[j]) > moduleDefIndentation: # we are in the function body  
                                templine1 = removeArgumentCtrlChars (srcArray[i].srcCode[j])
                                

                                if templine1.find("=") != -1: # this is a module instantiation 
                                    tokens = (templine1 [templine1.index ("=") + 1 : ]).split()
                                    for tokenIndex in xrange (0 , len (tokens)):
                                        if tokens[tokenIndex] == recArgName :
                                            recArgIndex = tokenIndex - 1
                                            module_name = tokens[0]                                
                                            recArgFound = 1
                                            # add the top module arguments info to the list in the recReg struct
                                            newRecRegs[recRegIndex]. RecRegHierarchy = newRecRegs[recRegIndex]. RecRegHierarchy + [ [module_name] + [recArgIndex] ]
                                
                            if  moduleDefFound == 1 and  indentation (srcArray[i].srcCode[j]) <= moduleDefIndentation: # gets out of the function body
                                moduleDefFound = 0  
                                break
                            templine2 = srcArray[i].srcCode[j]    
                            # find the module definition
                            if  moduleDefFound == 0 and isFunctionDefinition ((srcArray[i].srcCode[j]) , module_name) == 1 and (srcArray[i].srcCode[j]).find("=") != -1:
                                
                                # get the reconfigurable argument name
                                templine1 = removeArgumentCtrlChars (srcArray[i].srcCode[j])
                                tokens = templine1.split()
                                recArgName = tokens [recArgIndex + 1]
                                # get the indentation of the module definition line, the body indentation should be greater or equal to it
                                moduleDefIndentation = indentation (srcArray[i].srcCode[j])
                                moduleDefFound = 1
                            
    else : # there is only one reconfigurable region in the design 
        newRecRegs = recRegs                               
    return newRecRegs

# This function finds the reconfigurable-function argument in the hierarchy.
def populateRecRegions(srcArray,recRegs):
    if len(recRegs) > 1 : # if there are more than one rec region, trace them in the hierarchy, otherwise not needed.
        newSrcArray = srcArray
        moduleDefIndentation = 0
        for recRegIndex in xrange(0 , len (recRegs)):
            recRegs[recRegIndex].topModule = findRecTop(newSrcArray)
            # Find the reconfigurble argument index for the top module first
            module_name = recRegs[recRegIndex].topModule        
            for i in xrange (0 , len(newSrcArray)):        
                for j in xrange (0 , len(newSrcArray[i].srcCode)):
                    if isComment(newSrcArray[i].srcCode[j]) == 0 :
                        if (newSrcArray[i].srcCode[j]).find(recRegs[recRegIndex].RMVector) != -1 and (newSrcArray[i].srcCode[j]).find(recRegs[recRegIndex].topModule) != -1:
                            templine1 = replaceStrings([ ["("," "],[")"," "],[","," "],["!!"," "],["="," "]  ], newSrcArray[i].srcCode[j])
                            tokens = templine1.split()
                            for tokenIndex in xrange (0 , len (tokens)):
                                if tokens[tokenIndex] == recRegs[recRegIndex].topModule:
                                    topModuleIndex = tokenIndex
                                if tokens[tokenIndex] == recRegs[recRegIndex].RMVector :
                                    recArgIndex = int ( ( (tokenIndex -1) - topModuleIndex ) / 2 )

            while module_name != "recReg":
                # Find the module in the design hierarchy
                moduleDefFound = 0
                recArgFound = 0
                for i in xrange (0 , len(newSrcArray)): 
                    recRegFound = 0
                    importLines = []       
       
                    for j in xrange (0 , len(newSrcArray[i].srcCode)):
                        if isComment(newSrcArray[i].srcCode[j]) == 0 :

                            if  moduleDefFound == 1 and  indentation (newSrcArray[i].srcCode[j]) > moduleDefIndentation: # we are in the function body  
                                templine1 = removeArgumentCtrlChars (newSrcArray[i].srcCode[j])
                                

                                if templine1.find("=") != -1: # this is a module instantiation 
                                    tokens = (templine1 [templine1.index ("=") + 1 : ]).split()
                                    for tokenIndex in xrange (0 , len (tokens)):
                                        if tokens[tokenIndex] == recArgName :
                                            recArgIndex = tokenIndex - 1
                                            module_name = tokens[0]                                
                                            recArgFound = 1
                                            recRegFound = 1
                                            # if the module instantiation is a recReg module, change it to recReg + index in the source code
                                            if module_name == "recReg":
                                                newSrcArray[i].srcCode[j] = newSrcArray[i].srcCode[j].replace ("recReg", "recReg" + str(recRegIndex))
                                            break
                                            
                                
                            if  moduleDefFound == 1 and  indentation (newSrcArray[i].srcCode[j]) <= moduleDefIndentation: # gets out of the function body
                                moduleDefFound = 0  
                                break
                            templine2 = newSrcArray[i].srcCode[j]    
                            # find the module definition
                            if  moduleDefFound == 0 and isFunctionDefinition ((newSrcArray[i].srcCode[j]) , module_name) == 1 and (newSrcArray[i].srcCode[j]).find("=") != -1:
                                # get the reconfigurable argument name
                                templine1 = removeArgumentCtrlChars (newSrcArray[i].srcCode[j])
                                tokens = templine1.split()
                                recArgName = tokens [recArgIndex + 1]
                                # get the indentation of the module definition line, the body indentation should be greater or equal to it
                                moduleDefIndentation = indentation (newSrcArray[i].srcCode[j])
                                moduleDefFound = 1
                    # append the definition of recReg(k) to the file regRec is detected
                    if module_name == "recReg":
                        importLines = []
                        if recRegFound == 1 :
                            newSrcArray[i].srcCode = newSrcArray[i].srcCode + [ "\n" ]
                            newSrcArray[i].srcCode = newSrcArray[i].srcCode + [ "recReg" + str(recRegIndex) + " sel  inp = out \n" ]
                            newSrcArray[i].srcCode = newSrcArray[i].srcCode + [ "   where \n" ]                            
                                           
                            for indexRecReg in xrange (0 , recRegs[recRegIndex].NumOfRecModules):                        
                                if indexRecReg!=0 :
                                    newSrcArray[i].srcCode = newSrcArray[i].srcCode + [ "                 | (sel == " + str(indexRecReg) + ") = o" + str(indexRecReg) + "\n" ]
                                else:
                                    newSrcArray[i].srcCode = newSrcArray[i].srcCode + [ "       outIsoIn  | (sel == " + str(indexRecReg) + ") = o" + str(indexRecReg) + "\n"]    
                            newSrcArray[i].srcCode = newSrcArray[i].srcCode + [ "                | otherwise = o0 \n"] # default selectd module 

                            for indexRecReg in xrange (0 , recRegs[recRegIndex].NumOfRecModules):
                                newSrcArray[i].srcCode = newSrcArray[i].srcCode + [ "       o" + str(indexRecReg) + " = " + recRegs[recRegIndex].RecModules[indexRecReg]+ " inp \n" ]
                            newSrcArray[i].srcCode = newSrcArray[i].srcCode + [ "\n" ]
                            newSrcArray[i].srcCode = newSrcArray[i].srcCode + [ "out = recRegIsolation  sel outIsoIn\n"] # default selectd module                             
                            # add the rec module imports to the start of the array
                            for indexRecReg in xrange (0 , recRegs[recRegIndex].NumOfRecModules):
                                importLines = importLines + [ "import " +  capitalizeFirstChar(recRegs[recRegIndex].RecModules[indexRecReg]) + "\n"]
                            newSrcArray[i].srcCode = addImportLines(importLines , newSrcArray[i].srcCode) 
                            recRegFound = 0  
                             
                    if module_name == "recReg": # recReg found, get out of the loop
                        break

    else : # there is only one rec region, just find the recReg instance, change it to recReg0, and add vector funcitons to it
        newSrcArray = srcArray 
        recRegIndex = 0               
        for i in xrange (0 , len(newSrcArray)): 
            recRegFound = 0
            importLines = []       
            for j in xrange (0 , len(newSrcArray[i].srcCode)):
                if isComment(newSrcArray[i].srcCode[j]) == 0 :
                    templine = onlyLetters(newSrcArray[i].srcCode[j])
                    if newSrcArray[i].srcCode[j].find("=")!= -1 and newSrcArray[i].srcCode[j].find("recReg")!= -1 :
                        if newSrcArray[i].srcCode[j].index("=") < newSrcArray[i].srcCode[j].index("recReg"):
                            if isToken("recReg",templine):
                                newSrcArray[i].srcCode[j] = newSrcArray[i].srcCode[j].replace ("recReg", "recReg0")
                                recRegFound = 1

            if recRegFound == 1 :
                newSrcArray[i].srcCode = newSrcArray[i].srcCode + [ "\n" ]
                newSrcArray[i].srcCode = newSrcArray[i].srcCode + [ "recReg0"  + " sel  inp = out \n" ]
                newSrcArray[i].srcCode = newSrcArray[i].srcCode + [ "   where \n" ]                            
                newSrcArray[i].srcCode = newSrcArray[i].srcCode + [ "       outIsoIn  = recModMux (sel, vec0)\n"]
                for indexRecReg in xrange (0 , recRegs[recRegIndex].NumOfRecModules):                        
                   newSrcArray[i].srcCode = newSrcArray[i].srcCode + [ "       o" + str(indexRecReg)+ " = "+ recRegs[recRegIndex].RecModules[indexRecReg]+ " inp "+"\n"]    
                
                newSrcArray[i].srcCode = newSrcArray[i].srcCode + [ "       vec0 = bundle (o0 :>\n"]
                for indexRecReg in xrange (1 , recRegs[recRegIndex].NumOfRecModules):                        
                   newSrcArray[i].srcCode = newSrcArray[i].srcCode + [ "                      o" + str(indexRecReg)+ " :> \n"]    
                newSrcArray[i].srcCode = newSrcArray[i].srcCode + [ "                      Nil) \n"]       

                newSrcArray[i].srcCode = newSrcArray[i].srcCode + [ "       out = recRegIsolation  sel outIsoIn\n"] # default selectd module                             
                # add the rec module imports to the start of the array
                for indexRecReg in xrange (0 , recRegs[recRegIndex].NumOfRecModules):
                    importLines = importLines + [ "import " +  capitalizeFirstChar(recRegs[recRegIndex].RecModules[indexRecReg]) + "\n"]
                 
                newSrcArray[i].srcCode = addImportLines(importLines , newSrcArray[i].srcCode)
                
                recRegFound = 0  

    return newSrcArray



def printRecRegs(recRegs):
    for t in range (0,len(recRegions)):
        print "Reconfigurable Region %d" %(t + 1)
        print "        Vector Name  : %s" %(recRegions[t].RMVector)
        print "        Input Type   : %s" %(recRegions[t].InType)
        print "        Input Size   : %d" %(recRegions[t].InSize)
        print "        Output Type  : %s" %(recRegions[t].OutType)
        print "        Output Size  : %d" %(recRegions[t].OutSize)
        print "        Number of RMs: %d" %(recRegions[t].NumOfRecModules)
        print "        RMs          : %s" %(recRegions[t].RecModules)
        print "        Rec.Reg.Name : %s" %(recRegions[t].RecRegionName)    
    return 1

def printRecRegsHierarchy(recRegs):

    for i in xrange (0 , len (recRegs)):
        print "Hierarchy of reconfigurable region" + str (i)
        print recRegs[i]. RecRegHierarchy


    return 1

def setupModelSimSimulation(CLaSHModuleName,recRegions):
    
    # Copy the testbench file into the hdl source directory
    if (VERBOSE):
        print "Making a testbench file ... "
    
    tb_file_name = RTR_VHDL_DESIGN_SIM_HOME + CLaSHModuleName + "/tb.vhdl"
    topWrapper_file_name = RTR_VHDL_DESIGN_SIM_HOME + CLaSHModuleName + "/" + RTR_WRAPPER_NAME + ".vhdl"
    

    tb_file = open (tb_file_name ,"w")
    topWrapper_file = open (topWrapper_file_name ,"r")


    topWrapper_file_src = SourceFile()
    topWrapper_file_src.fileName = topWrapper_file_name
    for line in topWrapper_file:
        topWrapper_file_src.srcCode = topWrapper_file_src.srcCode + [str(line)]


    # Copy until the signal declaration area
    tb_file.write("library ieee;\n")
    tb_file.write("use ieee.std_logic_1164.all;\n")
    tb_file.write("use ieee.numeric_std.all;\n")    
    tb_file.write("use IEEE.NUMERIC_STD.ALL;\n")    
    tb_file.write("use work.all;\n")        
    
    
    tb_file.write("\n")    
    tb_file.write("entity tb is\n")    
    tb_file.write("end;\n")                                
    tb_file.write("\n")        
    tb_file.write("\n")                
    tb_file.write("architecture rtl of tb is\n")        
    tb_file.write("\n")
    tb_file.write("\n")    
    tb_file.write("-- Signal declaration goes here \n") 
    
    portFound = 0
    for ind in xrange (0,len(topWrapper_file_src.srcCode)) :
        if len(topWrapper_file_src.srcCode[ind]) > 1:
            noIndentation = removeIndentation( topWrapper_file_src.srcCode[ind])                        

            if noIndentation[0:4] == "end;":
                portFound = 0    
                break                 

            if portFound == 1:
                if charCount(noIndentation,")") > charCount(noIndentation,"("): # if it is the last line with closed parathesis
                    noIndentation = replaceStrings ([[");",";"]],noIndentation)           
                signalString = "Signal " + replaceStrings ([[" in "," "],[" out "," "], [" inout "," "]],noIndentation)    
                tb_file.write(signalString)    

            if noIndentation[0:5] == "port(":
                portFound = 1
                if charCount(noIndentation,")") > charCount(noIndentation,"("): # if it is the last line with closed parathesis
                    noIndentation = replaceStrings ([[");",";"]],noIndentation)
                signalString = "Signal " + replaceStrings ([[" in "," "],[" out "," "], [" inout "," "]] , noIndentation[5:]) 
                tb_file.write(signalString)    
            
    
    # start the port map
    tb_file.write("\n")
    tb_file.write("\n")                
    tb_file.write("begin\n")                                        
    tb_file.write("\n")                
    tb_file.write("topWrapperInst : entity topWrapper port map (\n" )                    

    portFound = 0
    for ind in xrange (0,len(topWrapper_file_src.srcCode)) :
        if len(topWrapper_file_src.srcCode[ind]) > 1:
            noIndentation = removeIndentation( topWrapper_file_src.srcCode[ind])                        
            tokens = noIndentation.split() 
            
            if noIndentation[0:4] == "end;":
               break                 

            if portFound == 1:
                signalString = "," + tokens[0] +  " => " + tokens[0] + "\n"
                tb_file.write(signalString)    
            if noIndentation[0:5] == "port(":
                portFound = 1
                tokens = noIndentation[5:].split()
                signalString = tokens[0] +  " => " + tokens[0] + "\n"
                tb_file.write(signalString)    
            


    tb_file.write(");\n")                                                                
    
    tb_file.write("\n")                
    tb_file.write("end rtl;\n")                

    if (VERBOSE):
        print "Done!"

    # Copy the clockgen module from include directory 
    src = RTR_CLASH_INCLUDE_PATH + "clkgen.vhdl"    
    dst = RTR_HASKELL_DESIGN_SIM_HOME + "vhdl/" + TOP_CLASH_MODULE_NAME + "/clkgen.vhdl"
    copyfile ( src , dst)

    # Copy isolation logic into the sim directory, specify the reconfiguration delay
    sim_isolation_file_name_src = RTR_CLASH_INCLUDE_PATH + "/recRegIsoCell.vhdl"
    sim_isolation_file_src = open (sim_isolation_file_name_src ,"r")
    
    sim_isolation_file_name_dst = RTR_HASKELL_DESIGN_SIM_HOME + "vhdl/" + TOP_CLASH_MODULE_NAME + "/recRegIsoCell.vhdl"
    sim_isolation_file_dst = open (sim_isolation_file_name_dst ,"w")
    
    for line in sim_isolation_file_src:
        
        line1 = line.replace ("#RTR_DELAY#",recRegions[0].RTRDelay)
        line2 = line1.replace ("#HIGH#",str(recRegions[0].OutSize-1))
        sim_isolation_file_dst.write(line2)
    
    
    # Create the simulation script for ModelSim, called sim.do here     
    sim_script_file_name = RTR_VHDL_DESIGN_SIM_HOME + CLaSHModuleName + "/sim.do"
    sim_do_file = open (sim_script_file_name ,"w")
    sim_do_file.write("vcom -work work -2002 -explicit -vopt *.vhdl\n")
    sim_do_file.write("vsim work.tb\n")
    sim_do_file.write("view -new wave\n")
    sim_do_file.write("add wave sim:/tb/*\n")    
    sim_do_file.write("run 1000 ms\n")
        
    return 1
def removeCurrentTop(srcArray):
    newSrcArray = srcArray
    
    # Go through all .hs files
    topAnnotationFound =0
    topDefinitionFound =0
    for i in xrange (0,len(srcArray)):
        for j in xrange (0 , len(srcArray[i].srcCode)):
            line = srcArray[i].srcCode[j]
            noSpaceLine = line.replace(" ","")
            # Look for Annotation
            if (noSpaceLine[0:15] == "{-#ANNtopEntity"): 
                topAnnotationFound =1
                line = " "
            if (topAnnotationFound ==1):
                line = " "
            if topAnnotationFound ==1 and noSpaceLine.find("#-}") != -1: 
                topAnnotationFound =0
                line = " "
                    

            # Look for topEntity definition
            
            if len(noSpaceLine) > 1:
                lineIndentation = indentation (line)
            else:
                lineIndentation = 0
                    
            if (noSpaceLine[0:10] == "topEntity="): 
                topDefinitionFound =1
                topIndentation = indentation(line)
                line = " "
            if (topDefinitionFound ==1):
                line = " "
            if topDefinitionFound ==1 and lineIndentation <= topIndentation: 
                topDefinitionFound =0
                line = " "
            
            newSrcArray[i].srcCode[j] = line
        
    return  newSrcArray  
    
def upperFirstChar(word):
    if len(word) > 1:       
        result = word[0].upper() + word[1:]
    else:
        result = word.upper()
        
    return result     
def makeRecModuleTop(srcArray, recModuleName,recRegNumber):
    newSrcArray = srcArray
    for i in xrange (0,len(srcArray)):
        # Look for the reconfigurable module
        if srcArray[i].fileName == upperFirstChar(recModuleName) + ".hs":
            # Add the top definition to it
            newSrcArray[i].srcCode = newSrcArray[i].srcCode + ["topEntity = " + recModuleName + "\n"]
            
            # Add the top Annotation to it
            
            newSrcArray[i].srcCode = newSrcArray[i].srcCode + ["{-# ANN topEntity" + "\n"]
            newSrcArray[i].srcCode = newSrcArray[i].srcCode + ["  (defTop" + "\n"]
            # Use the same name for Entity of all the reconfigurable modules of a reconfigurable region, they will replace the black-box in the static design
            newSrcArray[i].srcCode = newSrcArray[i].srcCode + ["    { t_name     = " + "\"" + "recReg" + str(recRegNumber) + "\"" + "\n"] 
            newSrcArray[i].srcCode = newSrcArray[i].srcCode + ["    , t_inputs   = [\"din\"] " + "\n"]
            newSrcArray[i].srcCode = newSrcArray[i].srcCode + ["    , t_outputs  = [\"dout\"]" + "\n"]
            newSrcArray[i].srcCode = newSrcArray[i].srcCode + ["    , t_extraIn  = [ (\"clk\",1), (\"rstn\",1)] " + "\n"]
            newSrcArray[i].srcCode = newSrcArray[i].srcCode + ["    , t_clocks   = [  clockWizard \"clkgen\" \"clk\" \"rstn\"] "]
            
            newSrcArray[i].srcCode = newSrcArray[i].srcCode + ["    }) #-}" + "\n"]                                                       
    
    return newSrcArray    
def isRecRegLocCosntraint(line):
    result = 0
    if len(line) > 2:        
        templine = line.split()
        if len(templine)>1:
            if templine[0][0:6] == "recReg":
                result = 1    
    return result
def isUserPortCosntraint(line):
    result = 0
    if len(line) > 2:        
        templine = line.split()
        if len(templine)>1:
            if templine[0].lower() == "connectlocal" and (templine[1] == "input" or templine[1] == "output"):
                result = 1    
    return result
def getHDLHierarchy(srcArray,recRegs):
    newRecRegs = recRegs
    # For each reconfigurable region, find the instance tree     
    for recind in xrange    (0, len(recRegs)):
        recRegs[recind].HDLHierarchy = []
        childModule = "recReg" + str(recind) 
        while childModule!= RTR_WRAPPER_NAME:
            # Find the module containing the instance 
            for i in xrange (0,len(srcArray)):
                for j in xrange (0 , len(srcArray[i].srcCode)):
                    line = srcArray[i].srcCode[j]                    
                    if len(line)>2:
                        templine = line.split()
                        if len(templine) > 2:
                            # Find the first entity definition in the file
                            if templine[0].lower() == "entity" and templine[2].lower() == "is": 
                                parentModule = templine[1]

                            if templine[1] == ":" and (templine[2].lower() == "entity" or templine[2].lower() == "component" ) and templine[3] == childModule :
                                # The Child instance is found, save the instance name in the recReg array
                                newRecRegs[recind].HDLHierarchy = [templine[0]] + newRecRegs[recind].HDLHierarchy
                                print "child module : " + childModule + "in file: " + srcArray[i].fileName
                                childModule = parentModule
                            

        # Add the topWrapper instance to the instance list for the reconfigurable region
        newRecRegs[recind].HDLHierarchy = ["topWrapperInst"] + newRecRegs[recind].HDLHierarchy
        
        if VERBOSE:
            print "Instance hierarchy for the reconfigurable region " + str(recind) + " : "
            print newRecRegs[recind].HDLHierarchy
                    
    return newRecRegs

def getHDLInstFile(srcArray,recreg):
    instFileName = ""
    for i in xrange (0,len(srcArray)):
        for j in xrange (0 , len(srcArray[i].srcCode)):
            line = srcArray[i].srcCode[j]                    
            if len(line)>2:
                templine = line.split()
                if len(templine) > 2:
                    # Find the first entity definition in the file

                    if templine[1] == ":" and (templine[2].lower() == "entity" or templine[2].lower() == "component") and templine[3] == recreg :
                        instFileName = srcArray[i].fileName
                        # The Child instance is found, save the instance name in the recReg array                            

                    
    return instFileName

def getPorts(filename):
    ports = []
    portFound = 0
    HDLfile = open(filename , "r")
    for line in HDLfile:
        if len(line) > 1:
            noIndentation = removeIndentation(line)                        

            if noIndentation[0:4] == "end;":
                portFound = 0    
                break                 

            if portFound == 1:
                if charCount(noIndentation,")") > charCount(noIndentation,"("): # if it is the last line with closed parathesis
                    noIndentation = replaceStrings ([[");",";"]],noIndentation)           
                signalString = noIndentation    
                ports = ports + [signalString]
            if noIndentation[0:5] == "port(":
                portFound = 1
                if charCount(noIndentation,")") > charCount(noIndentation,"("): # if it is the last line with closed parathesis
                    noIndentation = replaceStrings ([[");",";"]],noIndentation)
                signalString = noIndentation[5:]
                ports = ports + [signalString]
            
    return ports    
def isTopExternalPort(internalConnections,port):
    result = 1
    temp = (port.replace(":"," ")).split()
    portname = temp[0]
    
    for i in xrange (0,len(internalConnections)):
        tokens = internalConnections[i].split()
        if portname == tokens[2]:
            result = 0

    return result

def isSignedPort(port):
    temp1 = port.replace(":"," ")
    temp1 = temp1.replace("("," ")
    result = 0
    temp2 = temp1.split()
    for i in xrange (0,len(temp2)):
        if temp2[i].lower() == "signed":
            result = 1
    return result

def isUnsignedPort(port):
    temp1 = port.replace(":"," ")
    temp1 = temp1.replace("("," ")
    result = 0
    temp2 = temp1.split()
    for i in xrange (0,len(temp2)):
        if temp2[i].lower() == "unsigned":
            result = 1
    return result

def returnInternalConnection(internalConnections,port):
    temp = (port.replace(":"," ")).split()
    portname = temp[0]
    result = ""
    for i in xrange (0,len(internalConnections)):
        tokens = internalConnections[i].split()
        if portname == tokens[2]:
            if internalConnections[i].find(" input ") != -1:
                templine = internalConnections[i].replace(" input ","")
            else:
                templine = internalConnections[i].replace(" output ","")
            result = templine.replace ("connectLocal","")
            result = result.replace ("\n","")
            # Convert Unsigned/Signed to sld_logic_vector
            if isUnsignedPort(port):        
                result = portname + "_us \n"
            elif isSignedPort(port):    
                result = portname + "_s \n"
            else: # std_logic_vector
                result = result.replace (portname,"") + "\n"    
                
    return result

def returnTopPortConversions(internalConnections,port):
    temp = (port.replace(":"," ")).split()
    portname = temp[0]
    result = ""
    for i in xrange (0,len(internalConnections)):
        tokens = internalConnections[i].split()
        if portname == tokens[2]:
            if internalConnections[i].find(" input ") != -1:
                templine = internalConnections[i].replace(" input ","")
                
                right_side = internalConnections[i].replace(" input ","")
                right_side = right_side.replace("connectLocal","")
                right_side = right_side.replace(portname,"")                
                right_side = right_side.replace("\n","") 
                               
                # Convert Unsigned/Signed to sld_logic_vector
                if isUnsignedPort(port):
                    msb = port.replace("("," ")
                    msb = msb.replace(":"," : ")  
                    msbtokens = msb.split()                  
                    result = portname + "_us <= unsigned(" + right_side  + ");\n"
                elif isSignedPort(port):    
                    result = portname + "_s <= signed(" + right_side  +  ");\n"

                
            else:
                templine = internalConnections[i].replace(" output ","")
            
                left_side = internalConnections[i].replace(" output ","")
                left_side = left_side.replace("connectLocal","")
                left_side = left_side.replace(portname,"")   
                left_side = left_side.replace("\n","")                                             
                # Convert Unsigned/Signed to sld_logic_vector
                if isUnsignedPort(port):        
                    result = left_side + " <= std_logic_vector(" + portname + "_us" + ");\n"
                elif isSignedPort(port):    
                    result = left_side + " <= std_logic_vector(" + portname + "_s" + ");\n"
                
    return result


def setupSynthesis(srcArray, recRegs):
    # Make a syn directory
    deleteSynDir()
    makeSynDir()
    populateSynDir(srcArray)
    # Setup the static design: Copy the modified CLaSH source files and make a json file 
    # for the reconfigurable region.
    
    # Enter the CLaSH project directory
    os.chdir(RTR_HASKELL_DESIGN_SYN_HOME)

    # Run the full CLaSH command to translate the design to HDL
    print "Generating synthesizable static design ... "
    os.system(SYN_CLASH_COMMAND)
    print "Done!"    
    
    # Add the description for each reconfigurable region in the RecRegLib file.
    # The current top module and its annotations need to be removed from the design
    # and each reconfigurable module needs to be a top module and the HDL needs to be 
    # generated for that module.
    
    srcArrayWOTop = removeCurrentTop( removeRecRegDef(srcArray))
    
    for i in xrange (0, len(recRegs)):
        # Make the reconfigurable module top level
        for j in xrange (0, len (recRegs[i].RecModules)):
            #  Generate the HDL for the reconfigurable module
            RMTopSrcArray = makeRecModuleTop(srcArrayWOTop, recRegs[i].RecModules[j],i)
            
            # Write the source into files
            populateDir(RMTopSrcArray)
            
            # Generate the HDL
            print "Generating synthesizabLe desing for Reconfigurable Module: " + recRegs[i].RecModules[j] + " ..."
            CLASH_COMMAND = "clash " + upperFirstChar(recRegs[i].RecModules[j]) + ".hs" + " --" + HDL.lower()  
            os.system(CLASH_COMMAND)
            print "Done!"    
            
            # Copy the clockgen module from include directory 
            src = RTR_CLASH_INCLUDE_PATH + "clkgen.vhdl"    
            dst = RTR_HASKELL_DESIGN_SYN_HOME + "vhdl/" + upperFirstChar(recRegs[i].RecModules[j]) + "/clkgen.vhdl"
            copyfile ( src , dst)
        
        

    # Copy Vivado project template into the project folder
    # First delete the old one it if it exist    
    deleteDir(RTR_HASKELL_DESIGN_HOME + "/vivado_prj")
    
    
    fromDirectory = VIVADO_TEMPLATE_PRJ
    toDirectory = RTR_HASKELL_DESIGN_HOME + "vivado_prj"
    copy_tree(fromDirectory, toDirectory)
    

    # Define constraint files
    template_constraint_file_name = VIVADO_TEMPLATE_PRJ + "/project_template.srcs/constrs_1/new/boardcons.xdc"
    template_constraint_file = open (template_constraint_file_name ,"r")

    constraint_file_name = RTR_HASKELL_DESIGN_HOME + "vivado_prj/project_template.srcs/constrs_1/new/boardcons.xdc"
    constraint_file = open (constraint_file_name ,"w")
    
    # Copy the constraints from the template to the Vivado project
    for line in template_constraint_file:
        constraint_file.write(line)
    template_constraint_file.close()
    
    # Add user-defined constraints into the constraint file, and make a list of internal port connections (e.g. to the bus slave)
    user_constraint_file_name = RTR_HASKELL_DESIGN_HOME + "constraints.txt"
    user_constraint_file = open (user_constraint_file_name ,"r")
    internalConnections = []
    for line in user_constraint_file:
        if len(line) > 1:
            templine = line.split()
            if len(templine) > 1:
                
                # Check if it is a reconfigurable region location constraint            
                if isRecRegLocCosntraint(line):
                    # Store the location for the reconfigurable region
                    index = int( templine[0][6:])
                    recRegs[index].Location= templine[2]
                
                # Check if it is an internal connection constraint    
                elif isUserPortCosntraint(line):
                    internalConnections = internalConnections + [line]
                
                else:
                    # It is a Vivado constraint, write it to the constraint file directly
                    constraint_file.write(line)
            else:
                # It is a Vivado constraint, write it to the constraint file directly
                constraint_file.write(line)

        else:
            # It is a Vivado constraint, write it to the constraint file directly
            constraint_file.write(line)

    user_constraint_file.close()        
    
    # Find instance hierarchy for the reconfigurable regions
    HDLSrcArray = readHDLFiles()    
    newRecRegs = getHDLHierarchy(HDLSrcArray,recRegs)
    # Get user defined locations for the existing reconfigurable regions
    for recind in xrange (0, len(newRecRegs)) :
        
        # Get the instance hierarchy for the reconfigurable region
        instancePath = newRecRegs[recind].HDLHierarchy[0]
         
        for i in xrange (1, len(newRecRegs[recind].HDLHierarchy)):
            instancePath = instancePath + "/" + newRecRegs[recind].HDLHierarchy[i] 
      
        
        # Create a pblock for the reconfigurable region
        constraint_file.write("create_pblock pblock_rec" + str(recind)+ "\n")  
        
        # Specify the instantiation for the reconigurable region and add it to the reconfigurable block
        constraint_file.write("add_cells_to_pblock [get_pblocks pblock_rec" + str(recind) + "]  [get_cells -quiet [list " + instancePath + "]]"+ "\n")  
        
        # Define the Reconfigurable Region for Vivado and specify its location #
        constraint_file.write("resize_pblock [get_pblocks pblock_rec"+ str(recind) +"] -add "+ newRecRegs[recind].Location + "\n")  
        constraint_file.write("set_property RESET_AFTER_RECONFIG true [get_pblocks pblock_rec" + str(recind) + "]\n")  
        constraint_file.write("set_property SNAPPING_MODE ON [get_pblocks pblock_rec" + str(recind) +"]\n")
        constraint_file.write("set_property HD.RECONFIGURABLE true [get_cells " + instancePath +"]\n")        

    constraint_file.close()
    # The constraint file is complete now, copy it to the constraint directory in the vivado_pr project
    src = RTR_HASKELL_DESIGN_HOME + "vivado_prj/project_template.srcs/constrs_1/new/boardcons.xdc"
    dst = RTR_HASKELL_DESIGN_HOME + "vivado_prj/vivado_pr/constrs/boardcons.xdc"
    copyfile(src,dst)
    
    # Edit the Vivado project template and include the static part in the design wrapper
    # Open the design wrapper file from the template project and instantiate the static top in it, then write it to the vivado_prj folder in the copied template
    template_wrapper_file_name = VIVADO_TEMPLATE_PRJ + "/project_template.srcs/sources_1/imports/hdl/design_1_wrapper.vhd"
    template_wrapper_file = open (template_wrapper_file_name ,"r")
    
    new_wrapper_file_name = RTR_HASKELL_DESIGN_HOME + "vivado_prj/project_template.srcs/sources_1/imports/hdl/design_1_wrapper.vhd"
    new_wrapper_file = open (new_wrapper_file_name ,"w")
    
    # Get the ports of the topWrapper module    
    ports = getPorts(RTR_HASKELL_DESIGN_SYN_HOME + "vhdl/" + TOP_CLASH_MODULE_NAME + "/" + RTR_WRAPPER_NAME + ".vhdl")
    # Start copying the template file
    for line in template_wrapper_file:
        new_wrapper_file.write(line)

        # Instantiate all external ports (ones without internal connections), in the port list of the top module
        if line == "    --# User ports instantiated here #--\n":
            for i in xrange(0,len(ports)):
                if isTopExternalPort(internalConnections,ports[i]):
                    new_wrapper_file.write(ports[i] + "\n")

        if line == "  --# Signal definition for the static top goes here #--\n":
            for i in xrange(0,len(ports)):
                portstring = ports[i].replace(":", " : ")
                portstring = portstring.replace (" in "," ")
                portstring = portstring.replace (" out "," ")                
                if isTopExternalPort(internalConnections,ports[i]) == 0:
                
                    if ((portstring.replace("("," ")).lower()).find (" unsigned ") != -1:  
                        #ptemp = (portstring.lower()).replace(" unsigned"," std_logic_vector")
                        ptemp = portstring.replace (":"," : ")
                        ptokens = ptemp.split()
                        colind = ptemp.index(":")
                        newP = ptemp [colind:]
                        new_wrapper_file.write( "  signal " + ptokens[0]+"_us " + newP)
        
                    if ((portstring.replace("("," ")).lower()).find (" signed ") != -1:
                        #ptemp = (portstring.lower()).replace(" signed"," std_logic_vector")
                        ptemp = portstring.replace (":"," : ")
                        ptokens = ptemp.split()
                        colind = ptemp.index(":")
                        newP = ptemp [colind:]
                        new_wrapper_file.write( "  signal " + ptokens[0]+"_s " + newP)
          

  
        if line == "  --# Component declaration for the static top goes here #--\n":
            new_wrapper_file.write( "component " + RTR_WRAPPER_NAME +" port (\n" ) 
            for pind in xrange (0 , len(ports)):
                if pind == len(ports) -1 :
                    new_wrapper_file.write( ports[pind].replace(";",""))
                else:
                    new_wrapper_file.write(ports[pind])
            new_wrapper_file.write( ");\n")    
            new_wrapper_file.write( "end component;\n")       
                                               
        if line == "-- # Instantiate the top-level static design here # --\n":
            # Start instantiating the static top here                   
            new_wrapper_file.write(RTR_WRAPPER_NAME + "Inst : component " + RTR_WRAPPER_NAME +" port map (\n" )                    
            portConversions = []
            for i in xrange(0,len(ports)):
                templine = ports[i].replace(":","")
                templine2 = templine.split()                        

                # If it is an external port, bind it to the same name
                if isTopExternalPort(internalConnections,ports[i]):
                    if i == len(ports)-1 :
                        new_wrapper_file.write(templine2[0] + " => " + templine2[0] + "\n")
                    else:
                        new_wrapper_file.write(templine2[0] + " => " + templine2[0] + ",\n")
                # If it is an internal port, connect it to the signal specified in the user constraint file
                else: 
                    internal = returnInternalConnection(internalConnections,ports[i])
                    if i == len(ports)-1 :
                        new_wrapper_file.write(templine2[0] + " => " + internal + "\n")
                    else:
                        new_wrapper_file.write(templine2[0] + " => " + internal + ",") 
                    
                    if returnTopPortConversions (internalConnections,ports[i]) != "":
                        portConversions = portConversions + [returnTopPortConversions (internalConnections,ports[i])]                                           
                                        
            new_wrapper_file.write(");\n") 
            
            # Convert Unsigned/Signed signals to std_logic_vector and vice versa
            new_wrapper_file.write("\n\n")
            for portind in xrange (0,len(portConversions)):
                new_wrapper_file.write(portConversions[portind]) 


                                                                           
    new_wrapper_file.close()
    # Copy the static design HDL files into the source directory
    print "Copying HDL source files for the static design ... \n"

    fileList = findFiles( RTR_HASKELL_DESIGN_SYN_HOME + "vhdl/" + TOP_CLASH_MODULE_NAME , ".vhdl")
    
    for i in xrange (0,len(fileList)):
        src = RTR_HASKELL_DESIGN_SYN_HOME + "vhdl/" + TOP_CLASH_MODULE_NAME + "/" + fileList[i]
        dst = RTR_HASKELL_DESIGN_HOME + "/vivado_prj/project_template.srcs/sources_1/imports/hdl/" + fileList[i]
        copyfile(src, dst)    
    print "Done! \n"

    # Include the static design source files in the synthesis script before starting the sysnthesis of the static design
    src_syntclfile = open (VIVADO_TEMPLATE_PRJ + "/project_template.runs/synth_1/design_1_wrapper.tcl","r")    
    dst_syntclfile = open (RTR_HASKELL_DESIGN_HOME + "/vivado_prj/project_template.runs/synth_1/design_1_wrapper.tcl","w")
    
    for line in src_syntclfile:
        dst_syntclfile.write(line.replace("VIVADO_PRJ_PATH" , RTR_HASKELL_DESIGN_HOME + "vivado_prj"))
        if line == "read_vhdl -library xil_defaultlib {\n":
            for i in xrange (0,len(fileList)):
                dst_syntclfile.write ( RTR_HASKELL_DESIGN_HOME + "/vivado_prj/project_template.srcs/sources_1/imports/hdl/" + fileList[i] + "\n")
        
    
    # Copy the clockgen module from include directory 
    src = RTR_CLASH_INCLUDE_PATH + "clkgen.vhdl"
    dst = RTR_HASKELL_DESIGN_HOME + "/vivado_prj/project_template.srcs/sources_1/imports/hdl/clkgen.vhdl"
    copyfile(src, dst)  
    
    
    # Add the component for the recReg to the file it is instantiated. This is needed because it needs to be black box in static design
    ports = getPorts(RTR_HASKELL_DESIGN_SYN_HOME + "vhdl/" + upperFirstChar(recRegs[0].RecModules[0]) + "/" + "recReg0.vhdl")
    
    # Find the file in which the reconfigurable region is instatiated 
    instFileName = getHDLInstFile(HDLSrcArray,"recReg0")

    #Add the components to the file including the recreg instance
    instFile = open (RTR_HASKELL_DESIGN_HOME + "syn/vhdl/" + TOP_CLASH_MODULE_NAME + "/" + instFileName,"r")
    instFileEdited = open (RTR_HASKELL_DESIGN_HOME + "/vivado_prj/project_template.srcs/sources_1/imports/hdl/" + instFileName,"w")
    
    foundRecRegInst = 0 
    for line in instFile:        
        if len(line)>3:
            if (line.split())[0].lower() == "begin":
                
                # Declare the component here 
                instFileEdited.write("component recReg0 port( \n")
                for j in xrange (0,len(ports)):
                    if j == len(ports)-1:
                        instFileEdited.write( ports[j].replace(";",");" ) )
                    else:
                        instFileEdited.write(ports[j])                                 
                instFileEdited.write("end component;\n" )                
        
        
        
            tokens = (line.replace(",","")).split() 
            # In the same file, correct the clk and rst ports to be std_logic_vector
            if line.replace(" ","") == "recReg0_inst:componentrecReg0\n": # instantiation found
                foundRecRegInst = 1
            if  foundRecRegInst == 1:
                if len(tokens)> 1:
                    if tokens[0] == "clk" and tokens[1] == "=>":
                        if line.find(",") != -1:
                            line = "clk => (others => " +tokens[2]+ "),\n"
                        else:
                            line = "clk => (others => " +tokens[2]+ ")\n"
                                                                  
                    if tokens[0] == "rstn" and tokens[1] == "=>":
                        if line.find(",") != -1:
                            line = "rstn => (others => " +tokens[2]+ "),\n"
                        else:
                            line = "rstn => (others => " +tokens[2]+ ")\n"
                        foundRecRegInst = 0 # End the search for clk and reset ports on recReg0
                            
        instFileEdited.write(line)    
    return 1   
    
def runSynthesisForStatic():

    print "Running synthesis for the static design ..."            
    synLogFile = RTR_HASKELL_DESIGN_HOME + "vivado_prj/project_template.runs/synth_1/design_1_wrapper_utilization_synth.rpt"
    # Change the current directory into the synthesis directory
    os.chdir(RTR_HASKELL_DESIGN_HOME + "vivado_prj/project_template.runs/synth_1")

    # Run the Vivado synthesis command in batch mode, in a separate terminal, to get the netlist for static design
    proc = Popen(['xterm', '-e','vivado -mode batch -source  design_1_wrapper.tcl -notrace'])
    proc.wait()
    print "Synthesis finished, check the following synthesis report for further details:"
    print synLogFile
    # Copy the synthesized netlist as the design checkpoint into the vivado_pr project
    src = RTR_HASKELL_DESIGN_HOME + "/vivado_prj/project_template.runs/synth_1/design_1_wrapper.dcp"
    dst = RTR_HASKELL_DESIGN_HOME + "/vivado_prj/vivado_pr/srcs/dcp/design_1_wrapper.dcp"
    copyfile(src,dst)
    
    
    
    return 1
def adaptRTRScript(recRegs):

    # First clean the directories synth
    synPath = RTR_HASKELL_DESIGN_HOME + "vivado_prj/vivado_pr/"
    deleteDir(synPath + "Checkpoint")
    deleteDir(synPath + "hd_visual")
    deleteDir(synPath + "Implement")
    deleteDir(synPath + "Synth")
      
    # Make directories            
    makeDir(synPath + "Checkpoint")
    makeDir(synPath + "hd_visual")    
    makeDir(synPath + "Implement")
    makeDir(synPath + "Synth") 
    
    # Delete source directory for the reconfigurable modules
    recVariantsDir = RTR_HASKELL_DESIGN_HOME + "vivado_prj/vivado_pr/srcs/recvariants"
    deleteDir(recVariantsDir)
    # Make reconfigurable variant directories
    makeDir(recVariantsDir)
    
    
    # Get the instance hierarchy in the design
    HDLSrcArray = readHDLFiles()    
    newRecRegs = getHDLHierarchy(HDLSrcArray,recRegs)
    # Get user defined locations for the existing reconfigurable regions


    
    for i in xrange (0,len(recRegs)):
        for j in xrange (0,len(recRegs[i].RecModules)):
            # Make a directory for each reconfigurable module
            makeDir(recVariantsDir + "/" + recRegs[i].RecModules[j])
              
    # Copy the reconfigurable modules to the reconfigurable variants directories.
    for i in xrange (0,len(recRegs)):

        instancePath = newRecRegs[i].HDLHierarchy[0]         
        for reci in xrange (1, len(newRecRegs[i].HDLHierarchy)):
            instancePath = instancePath + "/" + newRecRegs[i].HDLHierarchy[reci] 
        for j in xrange (0,len(recRegs[i].RecModules)):
            # Get the existing hdl files in the reconfigurable module directory
            fileList = findFiles( RTR_HASKELL_DESIGN_SYN_HOME + "vhdl/" + upperFirstChar(recRegs[i].RecModules[j]), ".vhdl")
            srcdir = RTR_HASKELL_DESIGN_SYN_HOME + "vhdl/" + upperFirstChar(recRegs[i].RecModules[j]) + "/"
            destdir= recVariantsDir + "/" + recRegs[i].RecModules[j] + "/"
            
            # Copy hdl files from the reconfigurable module directory into the reconfigurable variant directory
            for k in xrange (0,len(fileList)):
                src = srcdir + fileList[k]
                dst = destdir + fileList[k]
                copyfile(src, dst) 
    
            # Make project file for each reconfigurable variant
            prj_file_name = RTR_HASKELL_DESIGN_HOME + "vivado_prj/vivado_pr/srcs/prj/" + recRegs[i].RecModules[j] + ".prj"
            prj_file = open (prj_file_name ,"w")
            
            # Write the hdl-source location for each reconfigurable variant in its project file
            for k in xrange (0,len(fileList)):
                # Write only if it is not a testbench file
                if fileList[k].find ("testbench.vhdl") == -1:
                    prj_file.write("vhdl xil_defaultLib ./srcs/recvariants/" + recRegs[i].RecModules[j] + "/" + fileList[k] + "\n")

    
    # Change the design.tcl script to apply new reconfigurable modules.
    template_tcl_file_name = VIVADO_TEMPLATE_PRJ + "/vivado_pr/scripts/design.tcl"
    template_tcl_file = open (template_tcl_file_name ,"r")

    tcl_file_name = RTR_HASKELL_DESIGN_HOME + "vivado_prj/vivado_pr/scripts/design.tcl"
    tcl_file = open (tcl_file_name ,"w")

    # Read from the template until it reaches the reconfigurable module specification
    for line in template_tcl_file:    
        tcl_file.write(line)
        if line == "# Add Reconfigurable cadidates here ################################\n":
            
            for recind in xrange (0,len(recRegs)):
                tcl_file.write("set module" + str(recind) + " \"recReg" + str(recind) + "\"\n")
                tcl_file.write("\n")
                
                for j in xrange (0,len(recRegs[recind].RecModules)):
                    tcl_file.write("set module" + str(recind)+ "_variant" + str(j) + " \"" + recRegs[recind].RecModules[j] + "\"\n")
                    tcl_file.write("set variant $module" + str(recind) + "_variant" + str(j) + "\n")  
                    tcl_file.write("add_module $variant\n")              
                    tcl_file.write("set_attribute module $variant moduleName   $module" + str(recind) + "\n")
                    tcl_file.write("set_attribute module $variant prj          $prjDir/$variant.prj\n")
                    tcl_file.write("set_attribute module $variant synth        ${run.rmSynth}\n")
                    tcl_file.write("\n")
                # Specify the reconfigurable region instance
                tcl_file.write("set module" + str(recind) + "_inst0 \"" + instancePath +"\" \n")
                # Write the configuration for each reconfigurable region

        if line == "# Add Configuration Implementation here ################################\n" :
            for recind in xrange (0,len(recRegs)):
                for j in xrange (0,len(recRegs[recind].RecModules)):
                    tcl_file.write("\n")
                    tcl_file.write("set config \"Config_${module" + str(recind) + "_variant" + str(j) + "}\" \n")
                    tcl_file.write("add_implementation $config \n")
                    tcl_file.write("set_attribute impl $config top             $top\n")
                    tcl_file.write("set_attribute impl $config pr.impl         1\n")
                    tcl_file.write("set_attribute impl $config implXDC         [list $xdcDir/boardcons.xdc]\n")
                    tcl_file.write("set_attribute impl $config impl            ${run.prImpl}\n")
                    
                    # Only implement the static part with the first reconfigurable module and reuse (import) for the other reconfigurable modules
                    if j==0:
                        tcl_file.write("set_attribute impl $config partitions      [list [list $static           $top           implement] \\"+"\n")
                    else:
                        tcl_file.write("set_attribute impl $config partitions      [list [list $static           $top           import] \\"+"\n") 

                        
                    tcl_file.write("                                                 [list $module" + str(recind)+ "_variant" + str(j) +" $module" + str(recind) + "_inst0 implement] \\"+"\n")
                    tcl_file.write("                                           ]\n")
                    tcl_file.write("set_attribute impl $config verify          ${run.prVerify}\n")
                    tcl_file.write("set_attribute impl $config bitstream       ${run.writeBitstream}\n")
                    tcl_file.write("set_attribute impl $config cfgmem.pcap     1\n")
                    tcl_file.write("\n")
                    
        
        if line == "# Add a flat implementation here ###################################\n":
            for recind in xrange (0,len(recRegs)): 
                tcl_file.write("add_implementation Flat\n")
                tcl_file.write("set_attribute impl Flat top          $top\n")
                tcl_file.write("set_attribute impl Flat implXDC      [list $xdcDir/boardcons.xdc]\n")
                tcl_file.write("set_attribute impl Flat partitions   [list [list $static           $top           implement] \\"+"\n")
                tcl_file.write("                                           [list $module" + str(recind) + "_variant0 $module0_inst0 implement] \\"+"\n")
                tcl_file.write("                                     ]\n")
                tcl_file.write("set_attribute impl Flat impl         ${run.flatImpl}\n")
 
    return 1
    
def generateBitstreams():
    os.chdir(RTR_VIVADO_DESIGN_HOME)
    #os.system(FULL_VIVADO_COMMAND)
    
    proc = Popen(['xterm', '-e',FULL_VIVADO_COMMAND])
    proc.wait()

    
    return 1
    
def generateRTRSoftware(recRegs):
    # Open the template software
    template_sw_file_name = VIVADO_TEMPLATE_PRJ + "/project_template.sdk/reconfig/src/reconfig.c"
    template_sw_file = open (template_sw_file_name ,"r")

    # Final reconfiguration management software
    sw_file_name = RTR_HASKELL_DESIGN_HOME + "vivado_prj/project_template.sdk/reconfig/src/reconfig.c"
    sw_file = open (sw_file_name ,"w")
 
    # Copy from the template file and customize it
    for line in template_sw_file:
        sw_file.write(line)
        if line == "////// Define addresses for reconfigurable modules in DDR RAM\n":
            for recind in xrange (0,len(recRegs)):
                for j in xrange (0,len(recRegs[recind].RecModules)): 
                    sw_file.write("#define " + recRegs[recind].RecModules[j] + "_addr_in_ddr 	XPAR_DDR_MEM_BASEADDR + " + str(j) + " * 0x60000U\n")
            
        
        if line == "    //Load the bitfiles to the DDR memory\n":
            for recind in xrange (0,len(recRegs)):
                for j in xrange (0,len(recRegs[recind].RecModules)): 
                    sw_file.write("status = SD_TransferPartial(\"" + recRegs[recind].RecModules[j] + ".bin\", " + recRegs[recind].RecModules[j] + "_addr_in_ddr,  BITFILE_LEN); \n")
                    sw_file.write("if (status != XST_SUCCESS) {xil_printf(\"failure\");\n")  
                    sw_file.write("return XST_FAILURE;}\n")    
    
    return 1                
# ***********************************************************************************************
#  Variables 
# ***********************************************************************************************

recRegions = []
srcArray = []

# **********************************************************************************************
#    Step 1: Run the CLaSH tool on the RTR design to genereate the VHDL/Verilog files. 
# ***********************************************************************************************

# Fill in the data structures ...
print (findSourceFiles())
print("Filling the data structures ...")
srcArray = fillDataStructure()


# Extract the reconfigurable regions
recRegions = findRecRegions(srcArray)

if VERBOSE == 1:
    printRecRegs(recRegions)

# Find the top  module
recTop = findRecTop(srcArray)
if VERBOSE == 1:
    print "Top Module Found: " + recTop
# create the new source array by changing the name of the reconfigurable regions    
completeRecRegs = findRecArgInHierarchy(srcArray,recRegions)   

# If there are more than one RR, find the hierarchy path. Not needed for one RR.
if len(recRegions) > 1 :
    printRecRegsHierarchy(completeRecRegs)

# create the new source array by changing the name of the reconfigurable regions    
populatedRecRegs = populateRecRegions(srcArray,completeRecRegs)   

# Fix the type of topEntity
fixedTop = changeTopmoduleType(recTop,populatedRecRegs)

# Comment out the wrapper module
readyToHDLSrc = commentWrapper(fixedTop,recTop)

# Clean the simulation directories if exist
deleteSimDir() 
   
# Make the simulation directory and copy the CLaSH source files into it
makeSimDir()

# Populate the simulation directory with HDL-convertable CLaSH source files
populateSimDir(readyToHDLSrc)


# Enter the CLaSH project directory
os.chdir(RTR_HASKELL_DESIGN_SIM_HOME)

# Run the full CLaSH command to translate the design to HDL
os.system(FULL_CLASH_COMMAND)

# Make a ModelSim simulation directory
setupModelSimSimulation(TOP_CLASH_MODULE_NAME,recRegions)

# Make the synthesis directory, fill in the static and reconfigurable hdl files
setupSynthesis(readyToHDLSrc , recRegions)

# Run synthesis for the static design, copy synthesized netlist of the static design into the vivado_pr (reconfigurable project) as a checkpoint
runSynthesisForStatic()

# Change the synthesis scripts for the reconfigurable part (design.tcl) to include all the reconfigurable modules
adaptRTRScript(recRegions)

# Run the design.tcl script to make full and partial bitstreams
generateBitstreams()

# Generate the reconfiguration management software
generateRTRSoftware(recRegions)

# Done :)


