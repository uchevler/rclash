{-# LANGUAGE RecordWildCards, DataKinds, MagicHash, TypeOperators#-}

module BasicBlk where
import CLaSH.Prelude

import Control.Applicative


data OperatorRepStType = Wait_st | Wait4InpData_Valid | Data_Detected_Once | Error_st
    deriving Eq 

data OperatorNoRepStType = WaitSt | ErrorSt
    deriving Eq 


toBV4:: (Unsigned 4) -> (BitVector 4)
toBV4 a = b
    where
        b = pack a

fromBV2Unsig4:: (BitVector 4) -> (Unsigned 4)
fromBV2Unsig4 a = b
        where 
            b = unpack a 

fromVec4BitUnsign4:: (Vec 4 Bit) -> (Unsigned 4)
fromVec4BitUnsign4 a = b
        where 
            b = unpack (pack a) 

vec4Bit2Unsign4TF:: Bit -> (Vec 4 Bit) -> (Bit, (Unsigned 4))
vec4Bit2Unsign4TF s a = (s',b)
        where 
            b = unpack (pack a) 
            s'= s

--vec4Bit2Unsign4:: (Vec 4 Bit) ->  (Unsigned 4) 
vec4Bit2Unsign4 = (vec4Bit2Unsign4TF <^> low)
-- This is the arbitration for activating FSMs
fsmActivation_TF :: Bit -> (Bit,Unsigned 4) ->  (Bit,Unsigned 4)
fsmActivation_TF s (startIn, active_Processes) = (s',activeprocessesnxt)
              where activeprocessesnxt | (startIn==high) =                  
                        case active_Processes of
                            0      ->   1  -- 0000 -> 0001 
                            1      ->   3  -- 0001 -> 0011
                            2      ->   3  -- 0010 -> 0011
                            3      ->   7  -- 0011 -> 0111
                            4      ->   5  -- 0100 -> 0101
                            5      ->   7  -- 0101 -> 0111
                            6      ->   7  -- 0110 -> 0111
                            7      ->   15 -- 0111 -> 1111
                            8      ->   9  -- 1000 -> 1001
                            9      ->   11 -- 1001 -> 1011
                            10     ->   11 -- 1010 -> 1011
                            11     ->   15 -- 1011 -> 1111
                            12     ->   13 -- 1100 -> 1101
                            13     ->   15 -- 1101 -> 1111
                            14     ->   15 -- 1110 -> 1111
                            15     ->   15 -- 1111 -> 1111

                                       | otherwise   = 0   
                    s'      =   low                                 

fsmActivation (startIn,active_Processes) = (fsmActivation_TF <^> low) (startIn,active_Processes) 


reduceOr4T dummyst (a ,b, c, d)  = (dummyst', res)
    where 
        res | (a ==high) || (b ==high) || (c ==high) || (d ==high) = high::Bit
            | otherwise                                = low
        dummyst'                                       = dummyst

reduceOr4 :: (Signal Bit , Signal Bit , Signal Bit , Signal Bit) -> (Signal Bit)  
reduceOr4 (a,b,c,d)  =  res
    where res        = (reduceOr4T <^> high) (a,b,c,d) 



reduceOr5T dummyst (a ,b, c, d,e)  = (dummyst', res)
    where 
        res | (a ==high) || (b ==high) || (c ==high) || (d ==high) || (e ==high) = high::Bit
            | otherwise                                           = low
        dummyst'                                                  = dummyst

reduceOr5 :: (Signal Bit , Signal Bit , Signal Bit , Signal Bit, Signal Bit ) -> (Signal Bit)  
reduceOr5 (a,b,c,d,e)  =  res
    where res          = (reduceOr5T <^> high) (a,b,c,d,e) 

putErrorOutTF dummyst (antecM,a ,b, c, d,e,f)  = (dummyst', res)
    where 
        res | (antecM ==low) && ((a ==high) || (b ==high) || (c ==high) || (d ==high) || (e ==high) || (f ==high)) = high::Bit
            | otherwise                                                      = low
        dummyst'                                                             = dummyst


putErrorOut (antecM,a,b,c,d,e,f)  =  res
        where res          = (putErrorOutTF <^> low) (antecM,a,b,c,d,e,f) 



putStartOut_TF dummyst (antecM,repInSeq, a ,b, c, d,e)  = (dummyst', res)
    where 
        res | (repInSeq ==high) && (antecM ==low) && ((a ==high) || (b ==high) || (c ==high) || (d ==high)) = high           
            | otherwise                                                                    = e
        dummyst'                                                                           = dummyst

putStartOut :: (Signal Bit,Signal Bit, Signal Bit , Signal Bit , Signal Bit , Signal Bit, Signal Bit ) -> (Signal Bit)  
putStartOut (antecM,repInSeq,a,b,c,d,e)  =  res
    where res          = (putStartOut_TF <^> low) (antecM,repInSeq,a,b,c,d,e) 

putValidRepOut_TF dummyst (antecM,a,b)  = (dummyst', res)
    where 
        res | (a ==high) && (antecM ==low)   = b
            | otherwise                 = 0
        dummyst'                        = dummyst

putValidRepOut (antecM,a,b)    =  res
    where res           = (putValidRepOut_TF <^> low) (antecM,a,b) 


---- FSM for the cases without any repitition operator in the sequence whic makes the sequence circuit very simple
fsmNoRep  ( operatorNoRepSt, startOutNoRepreg)  (antecModeIn, data_in, start_In, rep_In_Seq_In) = 
     
        case operatorNoRepSt of

             
                        WaitSt             ->  ((operatorNoRepSt_n,startOutNoRep_n), (stateOut,error_sig,startOutNoRep))
                          where

                             operatorNoRepSt_n   | (start_In == high) && (data_in == low) && (antecModeIn == low) && (rep_In_Seq_In == low )  = ErrorSt 
                                                 | otherwise                                                   = WaitSt
                             stateOut                                                                          = WaitSt
                             error_sig                                                                         = low

                             startOutNoRep_n     | (start_In == high) && (data_in == high)                     = high 
                                                 | otherwise                                                   = low 
                             startOutNoRep                                                                     = startOutNoRepreg

                        ErrorSt             ->  ((operatorNoRepSt_n,startOutNoRep_n), (stateOut,error_sig,startOutNoRep))
                          where

                             operatorNoRepSt_n                                                                 = ErrorSt
                             stateOut                                                                          = ErrorSt   
                             error_sig                                                                         = high  
                             startOutNoRep_n                                                                   = low  
                             startOutNoRep                                                                     = low



--------------------- The FSM for the sequences with the reptition operator in the sequence, this FSM is a bit complicated compared to the previous one 

fsmRep   (operatorRepSt,startOutreg,validRepOutreg)  (fsmIndex,data_in, start_In, error_In, rep_In_Seq_In, valid_Rep_In )= 
     
        case operatorRepSt of

             
                        Wait_st             ->  ((operatorRepSt_n,startOut_n,validRepOut_n), (stateOut,error_sig,startOut,validRepOut))
                          where

                             operatorRepSt_n     | (start_In==high) && (toBV4(valid_Rep_In)!fsmIndex == high) && (data_in == low) = Wait4InpData_Valid 
                                                 | (start_In==high) && (toBV4(valid_Rep_In)!fsmIndex == high) && (data_in == high) = Data_Detected_Once
                                                 | otherwise                                                             = Wait_st

                             startOut_n          | (start_In==high) && (toBV4(valid_Rep_In)!fsmIndex == high) && (data_in == high) = high
                                                 | otherwise                                                             = low

                             validRepOut_n       | (start_In==high) && (toBV4(valid_Rep_In)!fsmIndex == high) && (data_in == high) = high
                                                 | otherwise                                                             = low

                             startOut                                                                                    = startOutreg
                             validRepOut                                                                                 = validRepOutreg
                             stateOut                                                                                    = operatorRepSt   
                             error_sig                                                                                   = low             
                        Wait4InpData_Valid  ->  ((operatorRepSt_n,startOut_n,validRepOut_n), (stateOut,error_sig,startOut,validRepOut))
                          where

                             operatorRepSt_n     | (toBV4(valid_Rep_In)!fsmIndex == low)                                    = Error_st
                                                 | (start_In==high) && (toBV4(valid_Rep_In)!fsmIndex == high) && (data_in == high) = Data_Detected_Once
                                                 | otherwise                                                             = Wait4InpData_Valid

                             startOut_n          | (start_In==high) && (toBV4(valid_Rep_In)!fsmIndex == high) && (data_in == high) = high
                                                 | otherwise                                                             = low
                             validRepOut_n       | (start_In==high) && (toBV4(valid_Rep_In)!fsmIndex == high) && (data_in == high) = high
                                                 | otherwise                                                             = low

                             startOut                                                                                    = startOutreg
                             validRepOut                                                                                 = validRepOutreg
                             stateOut                                                                                    = operatorRepSt   
                             error_sig                                                                                   = low                                             
                        Data_Detected_Once   ->  ((operatorRepSt_n,startOut_n,validRepOut_n), (stateOut,error_sig,startOut,validRepOut))
                          where

                             operatorRepSt_n     | (toBV4(valid_Rep_In)!fsmIndex == low)                                 = Wait_st                                                 
                                                 | otherwise                                                             = Data_Detected_Once
                             startOut_n          | (start_In==high) && (toBV4(valid_Rep_In)!fsmIndex == high) && (data_in == high) = high
                                                 | otherwise                                                             = low
                             validRepOut_n       | (start_In==high) && (toBV4(valid_Rep_In)!fsmIndex == high) && (data_in == high) = high
                                                 | otherwise                                                             = low

                             startOut                                                                                    = startOutreg
                             validRepOut                                                                                 = validRepOutreg  
                             stateOut                                                                                    = operatorRepSt   
                             error_sig                                                                                   = low   
          
                        Error_st             ->  ((operatorRepSt_n,startOut_n,validRepOut_n), (stateOut,error_sig,startOut,validRepOut))
                          where
                             startOut_n                                                                                  = low
                             validRepOut_n                                                                               = low  
                             startOut                                                                                    = low
                             validRepOut                                                                                 = low  

                             stateOut                                                                                    = Error_st   

                             operatorRepSt_n                                                                             = Error_st

                             error_sig                                                                                   = high                                    

-- initial states for the state variables in the FSMs  
initFMSRep0   = (Wait_st::OperatorRepStType, low, low)
initFMSNoRep0 = (WaitSt::OperatorNoRepStType,low)

basicBlk:: (Signal Bit) -> (Signal Bit,Signal Bit, Signal Bit, Signal Bit, Signal (Unsigned 4)) -> (Signal Bit, Signal Bit, Signal Bit, Signal Bit, Signal (Unsigned 4))
basicBlk  data_in (antecModeIn,start_In, error_In, rep_In_Seq_In, valid_Rep_In )   = (antecModeOut,start_Out, error_Out, rep_In_Seq_out, valid_Rep_out)
    where
                         (stateOut0,error_sig0,startOut0,validRepOut0) = (fsmRep <^> initFMSRep0) ((signal 0),data_in, start_In, error_In, rep_In_Seq_In, valid_Rep_In )
                         (stateOut1,error_sig1,startOut1,validRepOut1) = (fsmRep <^> initFMSRep0) ((signal 1),data_in, start_In, error_In, rep_In_Seq_In, valid_Rep_In )
                         (stateOut2,error_sig2,startOut2,validRepOut2) = (fsmRep <^> initFMSRep0) ((signal 2),data_in, start_In, error_In, rep_In_Seq_In, valid_Rep_In )
                         (stateOut3,error_sig3,startOut3,validRepOut3) = (fsmRep <^> initFMSRep0) ((signal 3),data_in, start_In, error_In, rep_In_Seq_In, valid_Rep_In )

                         (stateOutNoRep,error_sigNorep,startOutNoRep0) = (fsmNoRep <^> initFMSNoRep0) (antecModeIn,data_in, start_In, rep_In_Seq_In )
             
                         start_Out                                     = putStartOut (antecModeIn,rep_In_Seq_In,startOut0,startOut1,startOut2,startOut3,startOutNoRep0)
                            
                         error_Out                                     = putErrorOut (antecModeIn,error_sig0,error_sig1,error_sig2,error_sig3,error_In,error_sigNorep)

                         rep_In_Seq_out                                = rep_In_Seq_In
                         
                         valid_Rep_out                                 = putValidRepOut(antecModeIn,rep_In_Seq_In,vec4Bit2Unsign4 (validRepOut0 :> validRepOut1 :> validRepOut2 :> validRepOut3 :> Nil))
                         antecModeOut                                  = antecModeIn

--- definition of the symbol for basicBlk
se = basicBlk
------- definition of the top entity used for VHDL translatoin  ----------------------------------- 


--topEntity:: (Signal Bit) -> (Signal Bit,Signal Bit, Signal Bit,Signal Bit,Signal (Unsigned 4)) -> (Signal Bit,Signal Bit, Signal Bit, Signal Bit, Signal (Unsigned 4))
--topEntity = basicBlk 

