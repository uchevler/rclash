{-# LANGUAGE TypeOperators, TypeFamilies, FlexibleContexts, ExplicitForAll, ScopedTypeVariables #-}

module BooleanOps where
import CLaSH.Prelude


-- false: False   true: True   
-- falseWeak: A weak false, while waiting because of an UNTIL or BEFORE  operator, the operator is evaluated to False because of the non-waiting operand in AND function. This propagates to other operations on it way to the IMPLICATION operator.
-- waiting: waiting because of an UNTIL or BEFORE operator.
 
type FourValue = BitVector 2

false :: FourValue
false = 00
 
true :: FourValue
true = 01

falseWeak :: FourValue
falseWeak = 10

waiting :: FourValue
waiting = 11


bitto4val:: Bit -> FourValue
bitto4val i | (i== 0)   =false
            | otherwise =true

lT128 a     | a < 128   = 1
            | otherwise = 0

lT128Bit a  | a < 128   = 1
            | otherwise = 0     
nEq0Bit a   | a /= 128  = 1
            | otherwise = 0

not4val :: FourValue -> FourValue
not4val a   | (a == true) = false                    
            | otherwise   = true

data OpState = S0|S1 deriving Eq
data BeforeStT = St0|St1|St2 deriving Eq
        
--- ==================== PSL-like operators are defined here ================================================================

------------------------  The Arrow notation is no longer required to chain operator-circuits. The verification elements  
------------------------  are connected to each other in tree structure automatically by using higher-order functions  


------------------ PSL-like AND circuit -----------------------

hask_AndTF s (a, b) = (s', o1)
  where 

    (s',o1)  | (s == S0) && (a ==false)                   = (S0,false)
             | (s == S0) && (a ==true) && (b ==true)      = (S0,true)
             | (s == S0) && (a ==true) && (b ==false)     = (S0,false)             
             | (s == S0) && (a ==true) && (b == waiting)  = (S1,waiting)
             
             | (s == S1) && (a ==false) &&
               ((b == falseWeak) || (b == waiting))       = (S1,falseWeak)
             | (s == S1) && (a ==true) && (b == waiting)  = (S1,waiting)
             | (s == S1) && (a ==true) && (b == falseWeak)= (S1,falseWeak)
             | (s == S1) && ((b ==true) || (b ==false))   = (S0,b)             
             | otherwise                                  = (S0,true)
  
hask_And (a,b) = out
        where
            out = (hask_AndTF <^> S0) (a,b)
            
------------------ PSL-like OR circuit -----------------------

hask_OrTF s (a ,b)  = (s', o1)
  where 

    (s',o1)  | (s == S0) && (a == true)                       = (S0,true)
             | (s == S0) && (a == false) && (b == false)      = (S0,false)
             | (s == S0) && (a == false) && (b == waiting)    = (S1,waiting)      
             
             | (s == S1) && (a == true) && (b == waiting)     = (S1,waiting)
             | (s == S1) && (a == true) && (b == falseWeak)   = (S1,waiting)
             | (s == S1) && (a == false) && (b == waiting)    = (S1,waiting)
             | (s == S1) && (a == false) && (b == falseWeak)  = (S1,falseWeak)
             | (s == S1) && ((b == true) || (b == false))     = (S0,b)                          
             | otherwise                                      = (S0,true)

hask_Or (a,b) = out
        where
            out = (hask_OrTF <^> S0) (a,b)
------------------ PSL-like NOT circuit -----------------------

hask_NotTF s a = (s', y1)
  where 
    y1    = not4val a
    s'    = 0

hask_Not i0 =   out
        where
            out = (hask_NotTF <^> 0) i0
------------------ PSL-like IMPLY circuit -----------------------
hask_ImplyTF s (a , b) = (s', y)
  where 

    
    (s',y)  | (s == S0) && (a == false)                          = (S0,true)
            | (s == S0) && (a == true) && (b == true)            = (S0,true)
            | (s == S0) && (a == true) && (b == false)           = (S0,false)
            | (s == S0) && (a == true) && (b == waiting)         = (S1,waiting)
            
            | (s == S1) && (a == true)   && (b == falseWeak)     = (S1,false)
            | (s == S1) && (a == true)   && (b == waiting)       = (S1,falseWeak)            
            | (s == S1) && (a == false)  && (b == falseWeak)     = (S1,waiting)
            | (s == S1) && (a == false)  && (b == waiting)         = (S1,waiting)
            | (s == S1) && ((b == false) || (b == true))         = (S0,b)
            | otherwise                                          = (S0, true)


hask_Imply (a,b) = out
        where
            out = (hask_ImplyTF <^> S0) (a,b)

------------------ PSL-like EVENTUALLY circuit, implements (a -> eventually b) expression -----------------------

hask_EventualTF s (a, b,eos) = (s', y)
  where 
    (s',y) | (s == S0) && (a == false)                = (S0, true)
           | (s == S0) && (a == true) && (b == true)  = (S0, true)
           | (s == S0) && (a == true) && (b /= true)  = (S1, waiting)           
           | (s == S1) && (b /= true) && (eos == false)= (S1, waiting) 
           | (s == S1) && (b == true) && (eos == false)= (S0, true) 
           | (s == S1) && (b /= true) && (eos == true) = (S0, false)                                  
           | otherwise = (S0, true)

hask_Eventual (a,b) eos = out
        where
            out = (hask_EventualTF <^> S0) (a,b,eos)
eventually = hask_Eventual
------------------ PSL-like equivalence circuit -----------------------
hask_EquivalentTF s0 (a, b) = (s0', y)
  where
    y   | (a == true)  && (b == true ) || 
          (a == false) && (b == false)    = true
        | otherwise                       = false
           
    s0' = 0
                
hask_Equivalent (a,b) = out
        where
            out = (hask_EquivalentTF <^> 0) (a,b)


------------------ PSL-like BEFORE circuit -----------------------
--- "a before b"  implementation
hask_BeforeTF  s (a, b) = (s', y)
  where     
    (s',y) | (s == S0) && (a == false) 
                       && (b == false)     = (S0, waiting)                       
           | (s == S0) && (a == false) 
                       && (b == true )     = (S0, false)
           | (s == S0) && (a == true ) 
                       && (b == true )     = (S0, false)
           | (s == S0) && (a == true ) 
                       && (b == false)     = (S1, true )
           | (s == S1) && (a == false) 
                       && (b == false)     = (S1, waiting)
           | (s == S1) && (a == true ) 
                       && (b == false)     = (S1, true)
           | (s == S1) && (a == true ) 
                       && (b == true )     = (S0, false)
           | (s == S1) && (a == false) 
                       && (b == true )     = (S0, false)
           | otherwise = (S0, true)
 

hask_Before (a,b) = out
        where
            out = (hask_BeforeTF <^> S0) (a,b)
            
before = hask_Before

------------------ PSL-like BEFORE_ circuit -----------------------
--- "a before_ b"  implementation
hask_Before_TF  s (a, b) = (s', y)
  where     
    (s',y) | (s == S0) && (a == false) 
                       && (b == false)     = (S0, waiting)                       
           | (s == S0) && (a == false) 
                       && (b == true )     = (S0, false)
           | (s == S0) && (a == true ) 
                       && (b == true )     = (S0, true )
           | (s == S0) && (a == true ) 
                       && (b == false)     = (S1, true )
           | (s == S1) && (a == false) 
                       && (b == false)     = (S1, waiting)
           | (s == S1) && (a == true ) 
                       && (b == false)     = (S1, true )
           | (s == S1) && (a == true ) 
                       && (b == true )     = (S0, true )
           | (s == S1) && (a == false) 
                       && (b == true )     = (S0, false)
           | otherwise = (S0, true)
 

hask_Before_ (a,b) = out
        where
            out = (hask_Before_TF <^> S0) (a,b)
            

------------------ PSL-like UNTIL circuit -----------------------


hask_UntilTF  s (a, b) = (s', y)
  where     
    (s',y)  | (s == S0) && (a == false )    = (S0, false)
    
            | (s == S0) && (a == true  ) 
                        && (b == true  )    = (S0, true)                        
            | (s == S0) && (a == true  ) 
                        && (b == false )    = (S1, waiting)                                    
            | (s == S1) && (a == true  ) 
                        && (b == false )    = (S1, waiting)
            | (s == S1) && (a == false ) 
                        && (b == false )    = (S0, false)
            | (s == S1) && (a == false ) 
                        && (b == true  )    = (S0, true)
            | (s == S1) && (a == true  ) 
                        && (b == true  )    = (S0, true)
            | otherwise                     = (S0, true )
            

hask_Until (a,b) = out
        where
            out = (hask_UntilTF <^> S0) (a,b)
            
untilC = hask_Until
------------------ PSL-like UNTIL_ circuit -----------------------


hask_Until_TF  s (a, b) = (s', y)
  where     
    (s', y) | (s == S0) && (a == false )    = (S0, false)    
            | (s == S0) && (a == true  ) 
                        && (b == true  )    = (S0, true)                        
            | (s == S0) && (a == true  ) 
                        && (b == false )    = (S1, waiting)                                    
            | (s == S1) && (a == true  ) 
                        && (b == false )    = (S1, waiting)
            | (s == S1) && (a == false ) 
                        && (b == false )    = (S0, false)
            | (s == S1) && (a == false ) 
                        && (b == true  )    = (S0, false)
            | (s == S1) && (a == true  ) 
                        && (b == true  )    = (S0, true)
            | otherwise                     = (S0, true )
            

hask_Until_ (a,b) = out
        where
            out = (hask_Until_TF <^> S0) (a,b)
untilC_ = hask_Until_          
            
------------------ PSL-like ALWAYS circuit -----------------------
-- the always operator checks the value of the expression considering the abort expression
hask_AlwaysTF  (s,abr) (expr , abortExpr) = ( (s',abr_n), y)
  where     
  
    abr_n | (s == S0) && (expr == waiting)               = abortExpr
          | (expr == falseWeak) && (abortExpr == false)  = false  
          | otherwise                                    = abr 

    (s', y) | (s == S0) && (expr ==false)
                        && (abr == false)               = (S0, false) 
            | (s == S0) && (expr == true)               = (S0, true ) 
            | (s == S0) && (expr == false)
                        && (abr == true)                = (S0, true) 
            | (s == S0) && (expr == waiting)            = (S1, waiting) 
            | (s == S1) && (expr == waiting)            = (S1, waiting) 
            | (s == S1) && (expr == false) && (abr == true) 
                        && (abortExpr == true)          = (S0, true) 
            | (s == S1) && (expr == false) && ((abr == false) 
                        || (abortExpr == false))        = (S0, false)              
            | otherwise                                 = (S0, true )


       
       
hask_Always expr  abortExpr = out
        where
            out = (hask_AlwaysTF <^> (S0,false)) (expr , abortExpr) 
always = hask_Always

------------------ PSL-like ABORT operator --------------------

hask_AbortTF s  a = (s', y)
  where 
    s' = 0
    y  | (a == false) = false
       | (a == true)  = true
       | otherwise    = false 
    
hask_Abort a = out
        where
            out = (hask_AbortTF <^> 0) a

abort = hask_Abort

------------------ PSL-like ROSE circuit -----------------------

hask_RoseTF s  a = (s', y)
  where 
    s' = a    
    y       | (s == false) && (a == true) = true     
            | otherwise     = false
    


hask_Rose a = out
        where
            out = (hask_RoseTF <^> 0) a

rising = hask_Rose
------------------ PSL-like Prev circuit -----------------------
hask_PrevTF :: FourValue -> FourValue -> (FourValue,FourValue)
hask_PrevTF s  a = (s', y)
  where 
    s' = a    
    y  = s


hask_Prev a = out
        where
            out = (hask_PrevTF <^> false) a

prev = hask_Prev
 
--------------- function to convert inputs to 4-value signals -------------------
sigTF s a = (s',out)
    where 
            out | (a ==True) = true
                | otherwise  = false     
            s'  = s
                
sig = (sigTF <^> S0)

-----------------------------------------  Logical Operators are defined here -----------------------------------------

(/\) a b = hask_And (a,b) 

(\/) a b = hask_Or (a,b)

(-->) a b = hask_Imply (a,b)    

(neg) a  = (hask_Not a) 

(<-->) a b = hask_Equivalent (a,b) 

prop1 a b c d =  always ((a /\ b) \/ c ) (abort d)
topEntity = prop1

--topEntity:: Signal FourValue -> Signal FourValue -> Signal FourValue -> Signal FourValue -> Signal FourValue
--topEntity a b c d = prop1 a b c d
-----------------
-- to test the list of functions idea ...
--testlist = [hask_And,hask_Or]
--testitem a b = testlist!!0 (a, b)    


