
module RepNM where
import CLaSH.Prelude


data OverlapCtrlStType = Wait_st | LessThan_N  | LessThan_M | Error_st 
    deriving Eq 



toBV4:: (Unsigned 4) -> (BitVector 4)
toBV4 a = b
    where
        b = pack a

fromBV2Unsig4:: (BitVector 4) -> (Unsigned 4)
fromBV2Unsig4 a = b
        where 
            b = unpack a 

fromVec4BitUnsign4:: (Vec 4 Bit) -> (Unsigned 4)
fromVec4BitUnsign4 a = b
        where 
            b = unpack (pack a) 

vec4Bit2Unsign4TF:: Bit -> (Vec 4 Bit) -> (Bit, (Unsigned 4))
vec4Bit2Unsign4TF s a = (s',b)
        where 
            b = unpack (pack a) 
            s'= s

--vec4Bit2Unsign4:: (Vec 4 Bit) ->  (Unsigned 4) 
vec4Bit2Unsign4 = (vec4Bit2Unsign4TF <^> low)
-- This is the arbitration for activating FSMs
fsmActivation_TF :: Bit -> (Bit,Unsigned 4) ->  (Bit,Unsigned 4)
fsmActivation_TF s (startIn, active_Processes) = (s',activeprocessesnxt)
              where activeprocessesnxt | (startIn==high) =                  
                        case active_Processes of
                            0      ->   1  -- 0000 -> 0001 
                            1      ->   3  -- 0001 -> 0011
                            2      ->   3  -- 0010 -> 0011
                            3      ->   7  -- 0011 -> 0111
                            4      ->   5  -- 0100 -> 0101
                            5      ->   7  -- 0101 -> 0111
                            6      ->   7  -- 0110 -> 0111
                            7      ->   15 -- 0111 -> 1111
                            8      ->   9  -- 1000 -> 1001
                            9      ->   11 -- 1001 -> 1011
                            10     ->   11 -- 1010 -> 1011
                            11     ->   15 -- 1011 -> 1111
                            12     ->   13 -- 1100 -> 1101
                            13     ->   15 -- 1101 -> 1111
                            14     ->   15 -- 1110 -> 1111
                            15     ->   15 -- 1111 -> 1111

                                       | otherwise   = 0   
                    s'      =   low                                 

fsmActivation (startIn,active_Processes) = (fsmActivation_TF <^> low) (startIn,active_Processes) 


reduceOr4T dummyst (a ,b, c, d)  = (dummyst', res)
    where 
        res | (a ==high) || (b ==high) || (c ==high) || (d ==high) = high::Bit
            | otherwise                                = low
        dummyst'                                       = low

reduceOr4 :: (Signal Bit , Signal Bit , Signal Bit , Signal Bit) -> (Signal Bit)  
reduceOr4 (a,b,c,d)  =  res
    where res        = (reduceOr4T <^> high) (a,b,c,d) 



reduceOr5T dummyst (a ,b, c, d,e)  = (dummyst', res)
    where 
        res | (a ==high) || (b ==high) || (c ==high) || (d ==high) || (e ==high) = high::Bit
            | otherwise                                           = low
        dummyst'                                                  = low

reduceOr5 :: (Signal Bit , Signal Bit , Signal Bit , Signal Bit, Signal Bit ) -> (Signal Bit)  
reduceOr5 (a,b,c,d,e)  =  res
    where res          = (reduceOr5T <^> high) (a,b,c,d,e) 




fsm   (overlapCtrlSt0 , counter0 , valid_Rep_inp_tmp0, active_Processes0)  (fsmIndex,antecMode,data_in, limitN, limitM, valid_Rep_In,activeProcessesNext )= 
     
        case overlapCtrlSt0 of

             
                        Wait_st     ->  ((overlapCtrlSt0_n , counter0_n , valid_Rep_out_tmp0_n, active_Processes0_n), (valid_Rep_out_tmp0, active_Processes0Out,error_sig_n))
                          where

                             counter0_n              | ((toBV4(activeProcessesNext)!fsmIndex == high) && (data_in == high))              = (counter0 + 1)                                                                
                                                     | otherwise                                                                         = 0

                             overlapCtrlSt0_n        | ((toBV4(activeProcessesNext)!fsmIndex == high) && (data_in == high))                     = LessThan_N 
                                                     | ((toBV4(activeProcessesNext)!fsmIndex == high) && (data_in == low) && (antecMode == low)) = Error_st                             
                                                     | otherwise                                                                         = Wait_st
                                       
                             valid_Rep_out_tmp0_n                                                                                        = low
                             valid_Rep_out_tmp0                                                                                          = valid_Rep_inp_tmp0
                                        
                             active_Processes0_n     | ((toBV4(activeProcessesNext)!fsmIndex == high) && (data_in == high))              = high                                                                
                                                     | otherwise                                                                         = low
                             active_Processes0Out                                                                                        = active_Processes0
                             error_sig_n                                                                                                 = low  



                        LessThan_N  ->  ((overlapCtrlSt0_n , counter0_n , valid_Rep_out_tmp0_n, active_Processes0_n), (valid_Rep_out_tmp0, active_Processes0Out,error_sig_n))

                             where

                               counter0_n            |  (data_in == high)                                            = counter0+1                                                     
                                                     |  otherwise                                                    = 0
 
                               overlapCtrlSt0_n      | (data_in == low) && (antecMode == low)                       = Error_st 
                                                     | (counter0 == limitN-1) && (data_in == high)                  = LessThan_M  
                                                     | (data_in == low) && (antecMode == high)                      = Wait_st 
                                                     | otherwise                                                    = LessThan_N

                               valid_Rep_out_tmp0_n  | (counter0 == limitN-1) && (data_in == high)                  = high                     
                                                     | otherwise                                                    = low 
                               valid_Rep_out_tmp0                                                                   = valid_Rep_inp_tmp0                              
                               
                               active_Processes0_n   | (counter0 == limitN-1) && (data_in == high)                  = high         
                                                     | otherwise                                                    = low 
                               active_Processes0Out                                                                 = active_Processes0  
                               error_sig_n                                                                          = low

                        LessThan_M  ->  ((overlapCtrlSt0_n , counter0_n , valid_Rep_out_tmp0_n, active_Processes0_n), (valid_Rep_out_tmp0, active_Processes0Out,error_sig_n))

                             where

                               counter0_n            | (counter0 <  limitM-1) && (data_in == high)  = counter0+1                                                     
                                                     | otherwise                                 = 0
 
                               overlapCtrlSt0_n      | (counter0 == limitM-1) || (data_in == low)  = Wait_st 
                                                     | otherwise                                 = LessThan_M

                               valid_Rep_out_tmp0_n  | (data_in == high)                         = high                     
                                                     | otherwise                                 = low 
                               valid_Rep_out_tmp0                                                = valid_Rep_inp_tmp0                              
                               
                               active_Processes0_n   | (counter0 <= limitM-1)                    = high 
                                                     | otherwise                                 = low

                               active_Processes0Out                                              = high
                               error_sig_n                                                       = low
    



                        Error_st    ->  ((overlapCtrlSt0_n , counter0_n , valid_Rep_out_tmp0_n, active_Processes0_n), (valid_Rep_out_tmp0, active_Processes0Out,error_sig_n))
                            where

                               overlapCtrlSt0_n     = Error_st
                               counter0_n           = 0

                               valid_Rep_out_tmp0_n = low
                               valid_Rep_out_tmp0   = low 

                               active_Processes0Out = low                                
                               active_Processes0_n  = low
                               error_sig_n          = high

               

----------------------- definition of the four parallel FSMs ...
initFMS0 = (Wait_st, (0::Unsigned 4), low::Bit, low::Bit)

fsm_initialized = (fsm <^> initFMS0)
sig0 = signal ( 0::(Unsigned 4))
sig1 = signal ( 1::(Unsigned 4))
sig2 = signal ( 2::(Unsigned 4))
sig3 = signal ( 3::(Unsigned 4))

reducOr4 = (reduceOr4T <^> low)
reducOr5 = (reduceOr5T <^> low)

repNM:: (Signal Bit, Signal (Unsigned 4),Signal (Unsigned 4)) -> (Signal Bit,Signal Bit, Signal Bit, Signal Bit, Signal (Unsigned 4)) -> (Signal Bit,Signal Bit, Signal Bit, Signal Bit, Signal (Unsigned 4))
repNM   (data_in,limitN,limitM) ( antecModeIn, start_In, error_In, rep_In_Seq_In, valid_Rep_In )   = (antecModeOut,start_Out, error_Out, rep_In_Seq_out, valid_Rep_out)
    where
             (valid_Rep_out_tmp0, active_Processes0,error_sig0) = (fsm_initialized)  (sig0,antecModeIn, data_in,limitN, limitM, valid_Rep_In, activeProcessesNext)
             (valid_Rep_out_tmp1, active_Processes1,error_sig1) = (fsm_initialized)  (sig1,antecModeIn, data_in,limitN, limitM, valid_Rep_In, activeProcessesNext)
             (valid_Rep_out_tmp2, active_Processes2,error_sig2) = (fsm_initialized)  (sig2,antecModeIn, data_in,limitN, limitM, valid_Rep_In, activeProcessesNext)
             (valid_Rep_out_tmp3, active_Processes3,error_sig3) = (fsm_initialized)  (sig3,antecModeIn, data_in,limitN, limitM, valid_Rep_In, activeProcessesNext)
             
             start_Out                                          = reduceOr4  (valid_Rep_out_tmp0,valid_Rep_out_tmp1,valid_Rep_out_tmp2,valid_Rep_out_tmp3)
             error_Out                                          = reduceOr5  (error_sig0,error_sig1,error_sig2,error_sig3,error_In)
             
             rep_In_Seq_out                                     = signal high
             valid_Rep_out                                      = vec4Bit2Unsign4 (valid_Rep_out_tmp0 :> valid_Rep_out_tmp1 :> valid_Rep_out_tmp2 :> valid_Rep_out_tmp3 :> Nil)
             activeProcessesNext                                = fsmActivation (start_In,vec4Bit2Unsign4 (active_Processes0 :> active_Processes1 :>active_Processes2 :> active_Processes3 :> Nil)) 
             antecModeOut                                       = antecModeIn                

(|~~|) i (n,m) = repNM (i,n,m) --- [=N] repConsN operator


------- definition of the top entity used for VHDL translatoin  ----------------------------------- 


--topEntity::(Signal Bit) -> (Signal (Unsigned 4) , Signal (Unsigned 4) ) -> (Signal Bit,Signal Bit, Signal Bit, Signal Bit, Signal (Unsigned 4)) -> (Signal Bit,Signal Bit, Signal Bit, Signal Bit,   Signal (Unsigned 4))
--topEntity = (|~~|)
