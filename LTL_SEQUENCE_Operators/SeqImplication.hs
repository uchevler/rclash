module SeqImplication where
import CLaSH.Prelude



--- definition of the sequence implication operator, it gets both right and left sides of the implication s input and combines them using some implication-specific modules.


--- seq4 datain = (repNM (datain,2,4)) . (repConsN (datain,2)) . (basicBlk (datain))

--(==>):: ((Signal Bit,Signal Bit, Signal Bit, Signal Bit, Signal (Unsigned 4)) -> (Signal Bit,Signal Bit, Signal Bit, Signal Bit, Signal (Unsigned 4)))  -> ((Signal Bit,Signal Bit, Signal Bit, Signal Bit, Signal (Unsigned 4)) -> (Signal Bit,Signal Bit, Signal Bit, Signal Bit, Signal (Unsigned 4))) -> ((Signal Bit,Signal Bit, Signal Bit, Signal Bit, Signal (Unsigned 4))) 
(==>)   a b = b.initConseq.a.initAntec

--- any sequence on the left side of the implication operator is initialized with this module.
initAntecTF::Bit -> ( Bit, Bit,  Bit,  Bit,  (Unsigned 4)) -> (Bit, ( Bit, Bit,  Bit,  Bit,  (Unsigned 4))) 
initAntecTF s (a,b,c,d,e)        = (s',(f,g,h,i,j))
            where 
                (f,g,h,i,j)    = ( high,  high,  low,  low,  (0::Unsigned 4))
                s'             = low
 
initAntec = (initAntecTF <^> low)
--- any sequence on the right side of the implication operator is initialized with this module, this module is a function itself apposed to initAntic which is a value 
initConseqTF::Bit -> ( Bit, Bit,  Bit,  Bit,  (Unsigned 4)) -> (Bit, ( Bit, Bit,  Bit,  Bit,  (Unsigned 4))) 
initConseqTF s (antecModeIn,start_In, error_In, rep_In_Seq_In, valid_Rep_In ) = (s', (antecModeOut,start_Out, error_Out, rep_In_Seq_out, valid_Rep_out))
    where 
           antecModeOut     =  low
           start_Out        =  start_In 
           error_Out        =  low
           rep_In_Seq_out   =  low
           valid_Rep_out    =  (0::(Unsigned 4))           
           s'               =  low 



initConseq = (initConseqTF <^> low)


--s2= seqf (f1,f2)
--data_in (antecModeIn,start_In, error_In, rep_In_Seq_In, valid_Rep_In )
--data_in = signal low
--data_in = (signal low)
--s1 = repNM (data_in,2,4) . repNM (data_in,2,4) 
--s2 = repNM (data_in,2,4) . repNM (data_in,2,4)

--s3= [s1,s2]


